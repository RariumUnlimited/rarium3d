#version 460
#extension GL_NV_gpu_shader5 : require
#extension GL_ARB_bindless_texture : require

//=======================================
//================<Datas>================
//=======================================

struct MaterialStruct {
    vec3 Ambient;
    vec3 Specular;
	uint Gloss;
    uint64_t Texture;
};

layout(shared, binding = 0) uniform GlobalDataBlock {
	mat4 ViewMatrix;
	mat4 ProjMatrix;
	vec2 ScreenResolution;
} GlobalData;

layout(shared, binding = 1) uniform LightDataBlock {
    vec4 Ambient;
    vec4 Power;
    vec4 Position;
    vec4 Direction;
    mat4 VPMatrix;
    uint64_t Texture;
} LightData;

//=======================================
//===============<Inputs>================
//=======================================

in VsOutBlock {
	vec2 TexCoord;
	vec3 WorldPos;
	vec3 Normal;
    vec4 ShadowCoord;
    flat MaterialStruct Material;
} FsIn;

//=======================================
//==============<Outputs>================
//=======================================

out vec4 color;

//=======================================
//=============<Functions>===============
//=======================================

vec3 getCameraPosition() {
    mat4 inverseView = inverse(GlobalData.ViewMatrix);
    return inverseView[3].xyz / inverseView[3].w;
}

void main() {
    vec3 normal = normalize(FsIn.Normal);
    vec4 material = texture(sampler2D(FsIn.Material.Texture), FsIn.TexCoord);

    // Ambient Intensity
    vec4 ambientColor = material * 
                        vec4(FsIn.Material.Ambient, 1.0) * 
                        LightData.Ambient;
    
    // Diffuse Intensity
    vec3 lightToPixel = normalize(FsIn.WorldPos - LightData.Position.xyz);
    float theta = dot(lightToPixel, normalize(LightData.Direction.xyz));
    vec4 diffuseColor = vec4(0.0, 0.0, 0.0, 1.0);
    vec4 specularColor = vec4(0.0, 0.0, 0.0, 1.0);
    
    if (theta > cos(LightData.Direction.w)) {
        diffuseColor = material * 
                       LightData.Power *
                       dot(normal, -lightToPixel) *
                       (1.0 - (1.0 - theta) /(1.0 - cos(LightData.Direction.w)));
                       
       // Specular Intensity
        vec3 pixelToEye = normalize(getCameraPosition() - FsIn.WorldPos);
        vec3 lightReflect = normalize(reflect(LightData.Direction.xyz, normal));
        float specularFactor = dot(pixelToEye, lightReflect);
        specularFactor = pow(specularFactor, FsIn.Material.Gloss);
        specularColor = material *
                        vec4(FsIn.Material.Specular, 1.0) * 
                        LightData.Power *
                        specularFactor;
    }
    
    // Shadow - Current Depth
    vec3 projCoords = FsIn.ShadowCoord.xyz / FsIn.ShadowCoord.w;
    projCoords = projCoords * 0.5 + 0.5; 
    float currentDepth = projCoords.z; 
    float bias = 0.005; 
    
    // Shadow - PCF
    sampler2D shadowMap = sampler2D(LightData.Texture);
    float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(shadowMap, 0);
    for(int x = -1; x <= 1; ++x)
    {
        for(int y = -1; y <= 1; ++y)
        {
            float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r; 
            shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;        
        }    
    }
    shadow /= 9.0;    
    
    // Final Color
    color = ambientColor + (1.0 - shadow) * diffuseColor;// + specularColor;
}