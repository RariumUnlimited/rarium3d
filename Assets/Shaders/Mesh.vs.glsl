#version 460
#extension GL_NV_gpu_shader5 : require
#extension GL_ARB_bindless_texture : require

//=======================================
//================<Datas>================
//=======================================

layout(shared, binding = 0) uniform GlobalDataBlock {
	mat4 ViewMatrix;
	mat4 ProjMatrix;
	vec2 ScreenResolution;
} GlobalData;

layout(shared, binding = 1) uniform LightDataBlock {
    vec4 Ambient;
    vec4 Power;
    vec4 Position;
    vec4 Direction;
    mat4 VPMatrix;
    uint64_t Texture;
} LightData;

struct MaterialStruct {
    vec3 Ambient;
    vec3 Specular;
	uint Gloss;
    uint64_t Texture;
};

//=======================================
//===============<Inputs>================
//=======================================

in vec4 Position;
in vec2 TexCoord;
in vec3 Normal;

in mat4 ModelMatrix;
in vec3 MatAmbient;
in vec3 MatSpecular;
in uint MatGloss;
in uint64_t MatTexture;

//=======================================
//==============<Outputs>================
//=======================================

out VsOutBlock {
	vec2 TexCoord;
	vec3 WorldPos;
	vec3 Normal;
    vec4 ShadowCoord;
    flat MaterialStruct Material;
} VsOut;

//=======================================
//=============<Functions>===============
//=======================================

void main() {
	VsOut.TexCoord = TexCoord;

	vec4 worldPos = ModelMatrix * Position;

	VsOut.WorldPos = worldPos.xyz;

	VsOut.Normal = mat3(ModelMatrix) * Normal;

    gl_Position = GlobalData.ProjMatrix * GlobalData.ViewMatrix * worldPos;
    
    VsOut.ShadowCoord = LightData.VPMatrix * worldPos;
    
    VsOut.Material.Gloss = MatGloss;
    VsOut.Material.Texture = MatTexture;
    VsOut.Material.Ambient = MatAmbient;
    VsOut.Material.Specular = MatSpecular;
}