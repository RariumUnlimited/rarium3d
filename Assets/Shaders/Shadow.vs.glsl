#version 460
#extension GL_NV_gpu_shader5 : require
#extension GL_ARB_bindless_texture : require

//=======================================
//================<Datas>================
//=======================================

layout(shared, binding = 1) uniform LightDataBlock {
    vec4 Ambient;
    vec4 Power;
    vec4 Position;
    vec4 Direction;
    mat4 VPMatrix;
    uint64_t Texture;
} LightData;

//=======================================
//===============<Inputs>================
//=======================================

in vec4 Position;

in mat4 ModelMatrix;

//=======================================
//==============<Outputs>================
//=======================================

//=======================================
//=============<Functions>===============
//=======================================

void main() {
    gl_Position = LightData.VPMatrix * ModelMatrix * Position;
}