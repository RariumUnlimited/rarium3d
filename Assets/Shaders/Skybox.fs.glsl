#version 460
#extension GL_NV_gpu_shader5 : require
#extension GL_ARB_bindless_texture : require

//=======================================
//================<Datas>================
//=======================================

//=======================================
//===============<Inputs>================
//=======================================

in VsOutBlock {
	vec2 TexCoord;
	vec3 WorldPos;
    flat uint64_t Texture;
} FsIn;

//=======================================
//==============<Outputs>================
//=======================================

out vec4 color;
layout (depth_greater) out float gl_FragDepth;

//=======================================
//=============<Functions>===============
//=======================================

void main() {
    vec3 diffuseColor = texture(sampler2D(FsIn.Texture), FsIn.TexCoord).rgb;
    color = vec4(diffuseColor, 1.0);
    gl_FragDepth = 1.0;
}