#version 460
#extension GL_NV_gpu_shader5 : require
#extension GL_ARB_bindless_texture : require

//=======================================
//================<Datas>================
//=======================================

layout(shared, binding = 0) uniform GlobalDataBlock {
	mat4 ViewMatrix;
	mat4 ProjMatrix;
	vec2 ScreenResolution;
} GlobalData;

//=======================================
//===============<Inputs>================
//=======================================

in vec4 Position;
in vec2 TexCoord;

in mat4 ModelMatrix;
in uint64_t MatTexture;

//=======================================
//==============<Outputs>================
//=======================================

out VsOutBlock {
	vec2 TexCoord;
	vec3 WorldPos;
    flat uint64_t Texture;
} VsOut;

//=======================================
//=============<Functions>===============
//=======================================

void main() {
	VsOut.TexCoord = TexCoord;

	vec4 worldPos = ModelMatrix * Position;

	VsOut.WorldPos = worldPos.xyz;

    gl_Position = GlobalData.ProjMatrix * GlobalData.ViewMatrix * worldPos;
    
    VsOut.Texture = MatTexture;
}