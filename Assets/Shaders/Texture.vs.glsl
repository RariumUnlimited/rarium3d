#version 450 core
#extension GL_NV_gpu_shader5 : require
#extension GL_ARB_bindless_texture : require

//=======================================
//================<Datas>================
//=======================================

layout(shared, binding = 0) uniform GlobalDataBlock {
	mat4 ViewMatrix;
	mat4 ProjMatrix;
	vec2 ScreenResolution;
} GlobalData;

//=======================================
//===============<Inputs>================
//=======================================

in vec2 Position;
in vec2 Size;
in float Z;
in uint64_t Texture;

//=======================================
//==============<Outputs>================
//=======================================

out VsOutBlock {
	vec2 TexCoord;
	flat uint64_t Texture;
	flat float Z;
} VsOut;

//=======================================
//=============<Functions>===============
//=======================================

void main() {
	vec2 tempPos = Position;
	vec2 sizeTemp = Size + Position;

	tempPos *= 2;
	tempPos -= GlobalData.ScreenResolution;
	tempPos /= GlobalData.ScreenResolution;


	sizeTemp *= 2;
	sizeTemp -= GlobalData.ScreenResolution;
	sizeTemp /= GlobalData.ScreenResolution;

	vec4 vertices[4] = vec4[4](
		vec4(tempPos.x,		tempPos.y,	1.0, 1.0),
		vec4(sizeTemp.x,	tempPos.y,	1.0, 1.0),
		vec4(tempPos.x,		sizeTemp.y, 1.0, 1.0),
		vec4(sizeTemp.x,	sizeTemp.y, 1.0, 1.0));
	
	vec2 tc[4] = vec2[4](
		vec2(0.0,0.0),
		vec2(1.0,0.0),
		vec2(0.0,1.0),
		vec2(1.0,1.0));

	vec4 pos = vertices[gl_VertexID];

	VsOut.TexCoord = tc[gl_VertexID];
	VsOut.Texture = Texture;
	VsOut.Z = Z;

    gl_Position = pos;
}
