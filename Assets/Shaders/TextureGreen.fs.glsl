#version 450 core
#extension GL_NV_gpu_shader5 : require
#extension GL_ARB_bindless_texture : require

//=======================================
//================<Datas>================
//=======================================

//=======================================
//===============<Inputs>================
//=======================================

in VsOutBlock {
	vec2 TexCoord;
	flat uint64_t Texture;
	flat float Z;
} FsIn;

//=======================================
//==============<Outputs>================
//=======================================

out vec4 color;
layout (depth_any) out float gl_FragDepth;

//=======================================
//=============<Functions>===============
//=======================================

void main() {
	gl_FragDepth = FsIn.Z;
	color = texture(sampler2D(FsIn.Texture), FsIn.TexCoord);
    
	if (color.a < 0.01)
		discard;

	color.r = 0.0;
	color.b = 0.0;
}
