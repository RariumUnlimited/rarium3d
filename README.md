libRarium.so
============

This repository holds the source code of the Rarium 3D Model Visualizer.

Features
--------

- Ui system with widgets : buttons, images, static and dynamic texts.
- Rendering using OpenGL 4.6

External Dependencies
---------------------

 - GLM
 - glbinding 2.1.1
 - SFML 2.5.1
 - OpenGL 4.6 with : GL_NV_gpu_shader5 and GL_ARB_bindless_texture

Build
-----

- Install [SFML 2.5.1](https://www.sfml-dev.org/download/sfml/2.5.1/), [GLM](https://github.com/g-truc/glm), [glbinding 2.1.1](https://github.com/cginternals/glbinding/releases/tag/v2.1.1)
- Clone using the `--recursive` flag to grab the dependencies

```
mkdir build
cd build
cmake ..
make 
./Rarium
```

License
-------

All librarium.so source code and assets are provided under the MIT license (See LICENSE file)
