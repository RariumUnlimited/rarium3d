// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_APP_ASTEROIDINSTANCE_H_

#define SRC_APP_ASTEROIDINSTANCE_H_

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Assets/Scene/MeshInstance.h"

namespace R3D {

/**
 * An instance of an texture
 */
struct AsteroidInstance : public MeshInstance {
    explicit AsteroidInstance(Mesh* mesh) :
        MeshInstance(mesh) {
        Scale = 1.f;
    }

    void Update(float delta) {
        Angle += Speed * delta;
        RotationAngle += RotationSpeed * delta;
        ModelMatrix = glm::scale(glm::mat4(1.f), glm::vec3(Scale, Scale, Scale));
        if (RotationAxis.z > 0.f)
            ModelMatrix = glm::rotate(glm::mat4(1.f), RotationAngle, glm::vec3(0.f, 0.f, RotationAxis.z)) * ModelMatrix;
        if (RotationAxis.y > 0.f)
            ModelMatrix = glm::rotate(glm::mat4(1.f), RotationAngle, glm::vec3(0.f, RotationAxis.y, 0.f)) * ModelMatrix;
        if (RotationAxis.x > 0.f)
            ModelMatrix = glm::rotate(glm::mat4(1.f), RotationAngle, glm::vec3(RotationAxis.x, 0.f, 0.f)) * ModelMatrix;
        ModelMatrix = glm::translate(glm::mat4(1.f), GetPosition()) * ModelMatrix;
    }

    inline glm::vec3 GetPosition() {
        return glm::vec3(Center.x + Radius * std::sin(Angle),
                         Center.y,
                         Center.z + Radius * std::cos(Angle));
    }

    // Polar coordinates, polar grid : https://en.wikipedia.org/wiki/File:Polar_graph_paper.svg
    float Angle;  ///< Angle on the polar grid in rad
    float Radius;  ///< Distance from the center of the polar grid
    glm::vec3 Center;  ///< Position of the center of the polar grid
    float Speed;  ///< Speed at which the asteroid move around the polar grid in rad/s
    float Scale;  ///< Scale of the asteroid

    // Self rotation
    float RotationAngle;  ///< Current rotation angle around itself
    float RotationSpeed;  ///< At which speed it rotate around itself
    glm::vec3 RotationAxis;  ///< Axis of rotation
};

}  // namespace R3D

#endif  // SRC_APP_ASTEROIDINSTANCE_H_
