// Copyright RariumUnlimited - Licence : MIT
#include "App/AsteroidScene.h"

#include <random>
#include <string>

#include <Rarium/Tools/Util.h>
#include <Rarium/Tools/Log.h>
#include <Rarium/Tools/Math.h>

#include "Assets/Collection.h"
#include "Assets/FileLoader.h"
#include "Assets/Technique/MeshTechnique.h"
#include "Assets/Technique/ShadowTechnique.h"
#include "Assets/Technique/SkyboxTechnique.h"

namespace R3D {

AsteroidSceneDescriptor::AsteroidSceneDescriptor(std::string name) :
    SceneDescriptor(name) {
}

std::unique_ptr<Scene> AsteroidSceneDescriptor::GetScene() {
    return std::make_unique<AsteroidScene>();
}

AsteroidUi::AsteroidUi() :
    backButton(this,
               "Back",
               sf::Vector2f(0.f, 0.f),
               RR::ButtonSprites{FileLoader::AsTexturePath("Button"),
                                 FileLoader::AsTexturePath("ButtonDark"),
                                 FileLoader::AsTexturePath("ButtonLight")},
               "Back",
               RR::TextCharacteristic{FileLoader::AsFontPath("redline"),
                                      30,
                                      sf::Color::White}) {
    backButton.Click += new RR::Event<void, RR::MouseEventArg>::T<AsteroidUi>(this,
                                                      &AsteroidUi::BackButtonClick);

    AddElement(&backButton);
}

void AsteroidUi::BackButtonClick(RR::MouseEventArg e) {
    windowFunction->RequestSceneChange("MainMenu");
}

AsteroidScene::AsteroidScene() :
    Scene(&cam, &myui),
    cam(80.f, 0.1f, 100.f,
         glm::uvec2(1280, 720), glm::vec3(0.f, 15.f, 15.f),
         glm::vec3(0.f, 0.f, 0.f), glm::vec3(0.f, 1.f, 0.f)) {
    lightData.Position.z = 27;
    lightData.Direction.w = glm::radians(30.f);

    lightData.VPMatrix = glm::perspective(glm::radians(30.f),
                                          1.f,
                                          2.f,
                                          100.f) *
                         glm::lookAt(glm::vec3(0.f, 0.f, 26.f),
                                     glm::vec3(0.f, 0.f, 0.f),
                                     glm::vec3(0.f, 1.f, 0.f));

    shadowPass = std::make_unique<ShadowPass>(lightData);

    passes.push_back(shadowPass.get());

    Collection::Techniques().Add<MeshTechnique>("Mesh");
    Collection::Techniques().Add<ShadowTechnique>("Shadow");
    Collection::Techniques().Add<SkyboxTechnique>("Skybox");

    std::vector<Mesh*> asteroidMesh;

    asteroidMesh.push_back(Collection::Meshes().Add("Asteroid_Small_1", "Asteroid_Small_1"));
    asteroidMesh.push_back(Collection::Meshes().Add("Asteroid_Small_2", "Asteroid_Small_2"));
    asteroidMesh.push_back(Collection::Meshes().Add("Asteroid_Small_3", "Asteroid_Small_3"));
    asteroidMesh.push_back(Collection::Meshes().Add("Asteroid_Small_4", "Asteroid_Small_4"));
    asteroidMesh.push_back(Collection::Meshes().Add("Asteroid_Small_5", "Asteroid_Small_5"));
    asteroidMesh.push_back(Collection::Meshes().Add("Asteroid_Small_6", "Asteroid_Small_6"));

    moon = std::unique_ptr<AsteroidInstance>(std::make_unique<AsteroidInstance>(asteroidMesh[0]));
    moon->Dynamic = true;
    moon->Scale = 7.f;

    moon->Center = glm::vec3(0.f, 0.f, 0.f);
    moon->Speed = 0.f;
    moon->Angle = 0.f;
    moon->Radius = 0.f;

    moon->RotationAngle = 0.f;
    moon->RotationSpeed = -0.2f;
    moon->RotationAxis = glm::vec3(0.f,
                                   1.f,
                                   0.f);

    instances.push_back(moon.get());

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> angleDistrib(0.f, 6.28f);
    std::uniform_real_distribution<float> altitudeDistrib(-2.f, 2.f);
    std::uniform_real_distribution<float> radiusDistrib(12.f, 25.f);
    std::uniform_real_distribution<float> speedDistrib(0.78539816339f, 1.57079632679f);
    std::uniform_int_distribution<int> asteroidDistrib(0, asteroidMesh.size() - 1);
    std::uniform_real_distribution<float> axisDistrib(0.f, 1.f);
    std::uniform_real_distribution<float> scaleDistrib(1.f, 1.5f);

    for (unsigned int i = 0; i < 50; ++i) {
        asteroids.emplace_back(std::make_unique<AsteroidInstance>(
                                   asteroidMesh[asteroidDistrib(gen)]));
        AsteroidInstance* aste = asteroids.back().get();
        aste->Dynamic = true;

        aste->Center = glm::vec3(0.f, altitudeDistrib(gen), 0.f);
        aste->Speed = speedDistrib(gen) / 2.f;
        aste->Angle = angleDistrib(gen);
        aste->Radius = radiusDistrib(gen);
        aste->Scale = scaleDistrib(gen);

        aste->RotationAngle = angleDistrib(gen);
        aste->RotationSpeed = speedDistrib(gen);
        aste->RotationAxis = glm::vec3(axisDistrib(gen),
                                      axisDistrib(gen),
                                      axisDistrib(gen));

        instances.push_back(aste);
    }

    std::vector<Mesh*> skyboxMesh;

    skyboxMesh.push_back(Collection::Meshes().Add("BoxBottom", "BoxBottom"));
    skyboxMesh.push_back(Collection::Meshes().Add("BoxTop", "BoxTop"));
    skyboxMesh.push_back(Collection::Meshes().Add("BoxFront", "BoxFront"));
    skyboxMesh.push_back(Collection::Meshes().Add("BoxBack", "BoxBack"));
    skyboxMesh.push_back(Collection::Meshes().Add("BoxLeft", "BoxLeft"));
    skyboxMesh.push_back(Collection::Meshes().Add("BoxRight", "BoxRight"));

    for (unsigned int i = 0; i < skyboxMesh.size(); ++i) {
        skybox.emplace_back(std::make_unique<MeshInstance>(skyboxMesh[i]));
        skybox.back()->Dynamic = true;
        skybox.back()->Technique = "Skybox";
        instances.push_back(skybox.back().get());
    }
}

void AsteroidScene::Update(float delta,
                       const RR::InputState& state,
                       const RR::InputState& lastState) {
    Scene::Update(delta, state, lastState);

    for (unsigned int i = 0; i < skybox.size(); ++i)
        skybox[i]->ModelMatrix = glm::translate(glm::mat4(), camera->GetPosition());

    for (unsigned int i = 0; i < asteroids.size(); ++i)
        asteroids[i]->Update(delta);

    moon->Update(delta);
}

}  // namespace R3D
