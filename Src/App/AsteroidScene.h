// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_APP_ASTEROIDSCENE_H_

#define SRC_APP_ASTEROIDSCENE_H_

#include <memory>
#include <string>
#include <vector>

#include <Rarium/Ui/UiElements/ButtonElement.h>

#include "App/AsteroidInstance.h"
#include "Assets/Scene/FixedCamera.h"
#include "Assets/Scene/MeshInstance.h"
#include "Assets/Scene/Scene.h"
#include "Assets/Scene/Ui.h"
#include "Render/Data/ShadowPass.h"

namespace R3D {

class AsteroidSceneDescriptor : public SceneDescriptor {
 public:
    explicit AsteroidSceneDescriptor(std::string name = "Demo3D");
    std::unique_ptr<Scene> GetScene() override;
};

class AsteroidUi : public Ui {
 public:
    AsteroidUi();

 protected:
    void BackButtonClick(RR::MouseEventArg e);

    RR::ButtonElement backButton;
};

class AsteroidScene : public Scene {
 public:
    AsteroidScene();
    void Update(float delta,
                const RR::InputState& state,
                const RR::InputState& lastState) override;

 protected:
    FixedCamera cam;
    AsteroidUi myui;

    std::unique_ptr<ShadowPass> shadowPass;

    std::unique_ptr<MeshInstance> asteroid1;
    std::unique_ptr<MeshInstance> asteroid2;
    std::unique_ptr<MeshInstance> asteroid3;
    std::unique_ptr<MeshInstance> asteroid4;
    std::unique_ptr<MeshInstance> asteroid5;
    std::unique_ptr<MeshInstance> asteroid6;
    std::unique_ptr<MeshInstance> asteroid7;
    std::unique_ptr<MeshInstance> asteroid8;
    std::unique_ptr<MeshInstance> asteroid9;


    std::unique_ptr<AsteroidInstance> moon;
    std::vector<std::unique_ptr<AsteroidInstance>> asteroids;

    std::vector<std::unique_ptr<MeshInstance>> skybox;
};

}  // namespace R3D

#endif  // SRC_APP_ASTEROIDSCENE_H_
