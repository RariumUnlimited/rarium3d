// Copyright RariumUnlimited - Licence : MIT
#include "App/GameScene.h"

#include <string>

#include <Rarium/Tools/Util.h>
#include <Rarium/Tools/Log.h>
#include <Rarium/Tools/Math.h>

#include "Assets/Collection.h"
#include "Assets/FileLoader.h"
#include "Assets/Technique/TextureTechnique.h"
#include "Assets/Technique/TextureGreenTechnique.h"

namespace R3D {

GameSceneDescriptor::GameSceneDescriptor(std::string name) :
    SceneDescriptor(name) {
}

std::unique_ptr<Scene> GameSceneDescriptor::GetScene() {
    return std::make_unique<GameScene>();
}

GameUi::GameUi() :
    backButton(this,
               "Back",
               sf::Vector2f(0.f, 0.f),
               RR::ButtonSprites{FileLoader::AsTexturePath("Button"),
                                 FileLoader::AsTexturePath("ButtonDark"),
                                 FileLoader::AsTexturePath("ButtonLight")},
               "Back",
               RR::TextCharacteristic{FileLoader::AsFontPath("redline"),
                                      30,
                                      sf::Color::White}) {
    backButton.Click += new RR::Event<void, RR::MouseEventArg>::T<GameUi>(this,
                                                      &GameUi::BackButtonClick);

    AddElement(&backButton);
}

void GameUi::BackButtonClick(RR::MouseEventArg e) {
    windowFunction->RequestSceneChange("MainMenu");
}

GameScene::GameScene() :
    Scene(&cam, &myui),
    cam(80.f, 0.1f, 100.f,
         glm::uvec2(1280, 720), glm::vec3(-3.f, -3.f, -3.f),
         glm::vec3(0.f, 0.f, 0.f), glm::vec3(0.f, 1.f, 0.f)),
    charac(Collection::Textures().Add("character","character"),
           glm::vec2(20 * celldim, 11 * celldim)) {
    Collection::Techniques().Add<TextureTechnique>("Texture");
    Collection::Techniques().Add<TextureGreenTechnique>("TextureGreen");

    xdim = 40;
    ydim = 23;
    celldim = 32;

    for (unsigned int x = 0; x < xdim; ++x) {
        for (unsigned int y = 0; y < ydim; ++y) {
            terrain.emplace_back(
                        std::make_unique<TextureInstance>(
                            Collection::Textures().Add("dirt", "dirt"),
                            glm::vec2(x * celldim, y * celldim)));
            if (x == 0 || y == 0 || x == xdim - 1 || y == ydim - 1) {
                terrain.back()->Technique = "TextureGreen";
            }
            instances.push_back(terrain.back().get());
        }
    }

    instances.push_back(&charac);
    charPos = glm::uvec2(20, 11);
    charac.Dynamic = true;
}

void GameScene::Update(float delta,
                       const RR::InputState& state,
                       const RR::InputState& lastState) {
    if (!state.LeftButtonDown && lastState.LeftButtonDown) {
        if (state.MousePosition.x > 0 &&
            state.MousePosition.x < 1280 &&
            state.MousePosition.y > 0 &&
            state.MousePosition.y < 720) {
            glm::ivec2 mousePos(state.MousePosition.x, state.MousePosition.y);
            mousePos.y = cam.GetViewport().y - mousePos.y;

            glm::vec2 mouseVec = glm::vec2(glm::vec2(mousePos) -
                                 (glm::vec2(charPos) *
                                  static_cast<float>(celldim) +
                                  static_cast<float>(celldim) / 2));
            mouseVec = normalize(mouseVec);
            float angle = glm::degrees(atan2(mouseVec.x,
                                        dot(glm::vec2(0.0, 1.0),
                                            mouseVec)));
            if (angle < 0)
                angle = 180 + (180 + angle);

            static const int start = -45 / 2;

            if (angle < start + 45) {  // N
                ++charPos.y;
            } else if (angle < start + 2 * 45) {  // NE
                ++charPos.y;
                ++charPos.x;
            } else if (angle < start + 3 * 45) {  // E
                ++charPos.x;
            } else if (angle < start + 4 * 45) {  // SE
                --charPos.y;
                ++charPos.x;
            } else if (angle < start + 5 * 45) {  // S
                --charPos.y;
            } else if (angle < start + 6 * 45) {  // SW
                --charPos.y;
                --charPos.x;
            } else if (angle < start + 7 * 45) {  // W
                --charPos.x;
            } else if (angle < start + 8 * 45) {  // NW
                ++charPos.y;
                --charPos.x;
            } else {  // N
                ++charPos.y;
            }

            charPos.x = RR::Clamp<unsigned int>(charPos.x, 1, xdim - 2);
            charPos.y = RR::Clamp<unsigned int>(charPos.y, 1, ydim - 2);
            std::get<TexData>(charac.Data).Position = glm::vec2(charPos.x * celldim,
                                                                charPos.y * celldim);
        }
    }
}

}  // namespace R3D
