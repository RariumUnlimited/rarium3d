// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_APP_GAMESCENE_H_

#define SRC_APP_GAMESCENE_H_

#include <memory>
#include <string>
#include <vector>

#include <Rarium/Ui/UiElements/ButtonElement.h>

#include "Assets/Scene/FixedCamera.h"
#include "Assets/Scene/Scene.h"
#include "Assets/Scene/TextureInstance.h"
#include "Assets/Scene/Ui.h"

namespace R3D {

class GameSceneDescriptor : public SceneDescriptor {
 public:
    explicit GameSceneDescriptor(std::string name = "Demo2D");
    std::unique_ptr<Scene> GetScene() override;
};

class GameUi : public Ui {
 public:
    GameUi();

 protected:
    void BackButtonClick(RR::MouseEventArg e);

    RR::ButtonElement backButton;
};

class GameScene : public Scene {
 public:
    GameScene();
    void Update(float delta,
                const RR::InputState& state,
                const RR::InputState& lastState) override;

 protected:
    FixedCamera cam;
    GameUi myui;

    unsigned int xdim;
    unsigned int ydim;
    unsigned int celldim;

    glm::uvec2 charPos;
    TextureInstance charac;
    std::vector<std::unique_ptr<TextureInstance>> terrain;
};

}  // namespace R3D

#endif  // SRC_APP_GAMESCENE_H_
