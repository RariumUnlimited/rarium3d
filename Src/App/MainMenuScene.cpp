// Copyright RariumUnlimited - Licence : MIT
#include "App/MainMenuScene.h"

#include <Rarium/Tools/Util.h>

#include "Assets/Collection.h"
#include "Assets/FileLoader.h"
#include "Assets/Instance.h"
#include "Assets/Technique/TextureTechnique.h"

namespace R3D {

MainMenuSceneDescriptor::MainMenuSceneDescriptor(std::string name) :
    SceneDescriptor(name) {
}

std::unique_ptr<Scene> MainMenuSceneDescriptor::GetScene() {
    return std::make_unique<MainMenuScene>();
}

MainMenuUi::MainMenuUi() :
    titleText(this, "Title", sf::Vector2f(430.f, 180.f), "librarium.so Demo",
              RR::TextCharacteristic{FileLoader::AsFontPath("redline"),
                                     50, 
                                     sf::Color::White}),
    demo1Button(this, "Start", sf::Vector2f(540.f, 420.f),
                RR::ButtonSprites{FileLoader::AsTexturePath("Button"),
                                  FileLoader::AsTexturePath("ButtonDark"),
                                  FileLoader::AsTexturePath("ButtonLight")},
                "2D Demo",
                RR::TextCharacteristic{FileLoader::AsFontPath("redline"),
                                       30,
                                       sf::Color::White}),
    demo2Button(this, "Start2", sf::Vector2f(540.f, 500.f),
                RR::ButtonSprites{FileLoader::AsTexturePath("Button"),
                                  FileLoader::AsTexturePath("ButtonDark"),
                                  FileLoader::AsTexturePath("ButtonLight")},
                "3D Demo",
                RR::TextCharacteristic{FileLoader::AsFontPath("redline"),
                                       30,
                                       sf::Color::White}) {
    AddElement(&titleText);
    AddElement(&demo1Button)->Click += new RR::Event<void, RR::MouseEventArg>::T<MainMenuUi>(this, &MainMenuUi::Demo2DButtonClick);
    AddElement(&demo2Button)->Click += new RR::Event<void, RR::MouseEventArg>::T<MainMenuUi>(this, &MainMenuUi::Demo3DButtonClick);
}

void MainMenuUi::Demo2DButtonClick(RR::MouseEventArg e) {
    windowFunction->RequestSceneChange("Demo2D");
}

void MainMenuUi::Demo3DButtonClick(RR::MouseEventArg e) {
    windowFunction->RequestSceneChange("Demo3D");
}

MainMenuScene::MainMenuScene() :
    Scene(&cam, &myui),
    cam(80.f, 0.1f, 100.f,
         glm::uvec2(1280, 720), glm::vec3(-3.f, -3.f, -3.f),
         glm::vec3(0.f, 0.f, 0.f), glm::vec3(0.f, 1.f, 0.f)),
    background(Collection::Textures().Add("Background", "Background"),
               glm::vec2(0.f, 0.f)) {

    Collection::Techniques().Add<TextureTechnique>("Texture");
    instances.push_back(&background);
}

}  // namespace R3D
