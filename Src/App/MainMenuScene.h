// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_APP_MAINMENUSCENE_H_

#define SRC_APP_MAINMENUSCENE_H_

#include <string>

#include <Rarium/Ui/UiElements/ButtonElement.h>
#include <Rarium/Ui/UiElements/TextElement.h>

#include "Assets/Scene/FixedCamera.h"
#include "Assets/Scene/Scene.h"
#include "Assets/Scene/TextureInstance.h"
#include "Assets/Scene/Ui.h"

namespace R3D {

class MainMenuSceneDescriptor : public SceneDescriptor {
 public:
    explicit MainMenuSceneDescriptor(std::string name = "MainMenu");
    std::unique_ptr<Scene> GetScene() override;
};


class MainMenuUi : public Ui {
 public:
    MainMenuUi();

 protected:
    void Demo2DButtonClick(RR::MouseEventArg e);
    void Demo3DButtonClick(RR::MouseEventArg e);

    RR::TextElement titleText;
    RR::ButtonElement demo1Button;
    RR::ButtonElement demo2Button;
};

class MainMenuScene : public Scene {
 public:
    MainMenuScene();

 protected:
    FixedCamera cam;
    MainMenuUi myui;

    TextureInstance background;
};

}  // namespace R3D

#endif  // SRC_APP_MAINMENUSCENE_H_
