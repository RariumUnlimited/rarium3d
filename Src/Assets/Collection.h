// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_ASSETS_COLLECTION_H_

#define SRC_ASSETS_COLLECTION_H_

#include <Rarium/Tools/Catalog.h>
#include <SFML/Graphics.hpp>

#include "Assets/Mesh.h"
#include "Assets/Program.h"
#include "Assets/Shader.h"
#include "Assets/Texture.h"
#include "Assets/Technique.h"

namespace R3D {

/// A collection of catalog
class Collection {
 public:
    static RR::Catalog<Program>& Programs() { return instance.programs; }
    static RR::Catalog<Shader>& Shaders() { return instance.shaders; }
    static RR::Catalog<Texture>& Textures() { return instance.textures; }
    static RR::Catalog<Technique>& Techniques() { return instance.techniques; }
    static RR::Catalog<sf::Font>& Fonts() { return instance.fonts; }
    static RR::Catalog<Mesh>& Meshes() { return instance.meshes; }

 private:
    Collection() {}
    RR::Catalog<Program> programs;
    RR::Catalog<Shader> shaders;
    RR::Catalog<Texture> textures;
    RR::Catalog<Technique> techniques;
    RR::Catalog<sf::Font> fonts;
    RR::Catalog<Mesh> meshes;

    static Collection instance;
};

}  // namespace R3D

#endif  // SRC_ASSETS_COLLECTION_H_
