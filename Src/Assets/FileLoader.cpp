// Copyright RariumUnlimited - Licence : MIT
#include "Assets/FileLoader.h"

#include <fstream>

namespace R3D {

std::string FileLoader::GetAssetsDir() {
    return "./Assets/";
}

std::string FileLoader::GetCachedAssetsDir() {
    return "./Cache/";
}

std::string FileLoader::AsTexturePath(const std::string& texture) {
    return GetAssetsDir() + "Textures/" + texture + ".png";
}

std::string FileLoader::AsFontPath(const std::string& font) {
    return GetAssetsDir()+"Fonts/" + font + ".ttf";
}

std::string FileLoader::AsShaderPath(const std::string& shader) {
    return GetAssetsDir()+"Shaders/" + shader + ".glsl";
}

std::string FileLoader::AsMeshPath(const std::string& mesh) {
    return GetAssetsDir()+"Meshes/" + mesh + ".ply";
}

std::string FileLoader::AsMaterialPath(const std::string& mat) {
    return GetAssetsDir()+"Meshes/" + mat + ".mat";
}

std::string FileLoader::AsCachedMeshPath(const std::string& mesh) {
    return GetCachedAssetsDir() + mesh + ".cmesh";
}

}  // namespace R3D
