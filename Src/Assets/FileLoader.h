// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_ASSETS_FILELOADER_H_

#define SRC_ASSETS_FILELOADER_H_

#include <string>
#include <vector>

#include <SFML/System.hpp>

namespace R3D {

/**
 *    Class used to load content from file, or get full path to an asset
 */
class FileLoader : public sf::NonCopyable {
 public:
    /**
     * Get the path to the assets directory.
     * @return The path to the assets directory.
     */
    static std::string GetAssetsDir();
    /**
     * Get the path to the cached assets directory.
     * @return The path to the cached assets directory.
     */
    static std::string GetCachedAssetsDir();

    /// Get the path to an asset that is a texture
    static std::string AsTexturePath(const std::string& texture);
    /// Get the path to an asset that is a font
    static std::string AsFontPath(const std::string& font);
    /// Get the path to an asset that is a shader
    static std::string AsShaderPath(const std::string& shader);
    /// Get the path to an asset that is a mesh
    static std::string AsMeshPath(const std::string& mesh);
    /// Get the path to an asset that is a material
    static std::string AsMaterialPath(const std::string& mat);
    /// Get the path to an cached asset that is a mesh
    static std::string AsCachedMeshPath(const std::string& mesh);
};

}  // namespace R3D

#endif  // SRC_ASSETS_FILELOADER_H_
