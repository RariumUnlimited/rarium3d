// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_ASSETS_INSTANCE_H_

#define SRC_ASSETS_INSTANCE_H_

#include <string>
#include <variant>

#include <glm/glm.hpp>

#include "Assets/Mesh.h"
#include "Assets/Texture.h"

namespace R3D {

/// Data of a texture instance
struct TexData {
    glm::vec2 Position;  ///< Position of the texture
    glm::vec2 Size;  ///< Size of the texture
    float Z;
    Texture* Tex;  ///< The texture
};

/// Data of a mesh instance;
struct MeshData {
    glm::mat4 ModelMatrix;  ///< Model matrix of the instance
    R3D::Mesh* Mesh;  ///< The mesh
};

/**
 *	An instance of an object of the scene
 */
struct Instance {
    enum class T { Text, Mesh } Type;  ///< Type of the technique

    typedef std::variant<TexData, MeshData> DataVariant;

    explicit Instance(T type) :
        Type(type) {
        Dynamic = false;
        switch (Type) {
        case T::Text:
            Data = DataVariant(std::in_place_type<TexData>);
            break;
        case T::Mesh:
            Data = DataVariant(std::in_place_type<MeshData>);
            break;
        }
    }

    DataVariant Data;  ///< Data of the instance

    std::string Technique;  ///< Name of the technique used to draw the instance
    bool Dynamic;  ///< If the instance is dynamic (it moves, ...)
};

}  // namespace R3D

#endif  // SRC_ASSETS_INSTANCE_H_
