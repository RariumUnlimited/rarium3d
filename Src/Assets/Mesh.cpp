// Copyright RariumUnlimited - Licence : MIT
#include "Assets/Mesh.h"

#include <filesystem>
#include <fstream>
#include <iostream>
#include <sstream>
#include <unordered_map>

#include <Rarium/Tools/FileLoader.h>
#include <Rarium/Tools/Util.h>

#include "Assets/FileLoader.h"
#include "Assets/Collection.h"

namespace R3D {

Mesh::Mesh(const std::string& mesh) :
    mesh(mesh) {
    if (std::filesystem::exists(FileLoader::AsCachedMeshPath(mesh)))
        LoadFromCache();
    else
        LoadFromPly();

    LoadMaterial();
}

void Mesh::LoadMaterial() {
    std::string matContent =
            RR::LoadFileAsString(FileLoader::AsMaterialPath(mesh), "");

    std::istringstream iss(matContent);

    std::string line;

    while (std::getline(iss, line)) {
        std::vector<std::string> split = RR::Split(line, ' ');

        if (split.size() > 0) {
            if (split[0] == "Ns") {
                material.Ns = std::stof(split[1]);
            } else if (split[0] == "Ka") {
                material.Ka.x = std::stof(split[1]);
                material.Ka.y = std::stof(split[2]);
                material.Ka.z = std::stof(split[3]);
            } else if (split[0] == "Ks") {
                material.Ks.x = std::stof(split[1]);
                material.Ks.y = std::stof(split[2]);
                material.Ks.z = std::stof(split[3]);
            } else if (split[0] == "map_Kd") {
                material.KdMap = Collection::Textures().Add(split[1], split[1]);
            }
        }
    }
}

void Mesh::LoadFromCache() {
    std::ifstream ifs(FileLoader::AsCachedMeshPath(mesh), std::ifstream::binary);

    if (!ifs.good())
         throw std::string("Could not read file : " +
                           FileLoader::AsCachedMeshPath(mesh));

    unsigned int vCount;
    unsigned int iCount;

    ifs.read(reinterpret_cast<char*>(&vCount), sizeof(unsigned int));
    ifs.read(reinterpret_cast<char*>(&iCount), sizeof(unsigned int));

    vertices.resize(vCount);
    indices.resize(iCount);

    ifs.read(reinterpret_cast<char*>(vertices.data()), sizeof(Vertex) * vCount);
    ifs.read(reinterpret_cast<char*>(indices.data()), sizeof(unsigned int) * iCount);

    ifs.close();
}

void Mesh::LoadFromPly() {
    std::string meshContent =
            RR::LoadFileAsString(FileLoader::AsMeshPath(mesh), "");

    std::istringstream iss(meshContent);

    std::string line;

    std::getline(iss, line);

    std::unordered_map<std::string, int> propertiesIndex = {
        {"x", -1},
        {"y", -1},
        {"z", -1},
        {"s", -1},
        {"t", -1},
        {"nx", -1},
        {"ny", -1},
        {"nz", -1}
    };

    int currentPropertyIndex = 0;

    while (line != "end_header") {
        std::vector<std::string> split = RR::Split(line, ' ');

        if (split[0] == "element") {
            if (split[1] == "vertex")
                vertices.resize(std::stoi(split[2]));
            else if (split[1] == "face")
                indices.resize(std::stoi(split[2]) * 3);
        } else if (split[0] == "property") {
            propertiesIndex[split[2]] = currentPropertyIndex++;
        }

        std::getline(iss, line);
    }

    bool foundAllProperty = true;

    for (auto& p : propertiesIndex) {
        if (p.second == -1)
            foundAllProperty = false;
    }

    if (!foundAllProperty || vertices.size() == 0 || indices.size() == 0)
        throw std::string("Mesh " + mesh +
                          " doesn't have all necessary properties");

    for (unsigned int i = 0; i < vertices.size(); ++i) {
        std::getline(iss, line);
        std::vector<std::string> split = RR::Split(line, ' ');

        Vertex& v = vertices[i];
        v.Position.x = std::stof(split[propertiesIndex["x"]]);
        v.Position.y = std::stof(split[propertiesIndex["y"]]);
        v.Position.z = std::stof(split[propertiesIndex["z"]]);
        v.TexCoord.x = std::stof(split[propertiesIndex["s"]]);
        v.TexCoord.y = std::stof(split[propertiesIndex["t"]]);
        v.Normal.x = std::stof(split[propertiesIndex["nx"]]);
        v.Normal.y = std::stof(split[propertiesIndex["ny"]]);
        v.Normal.z = std::stof(split[propertiesIndex["nz"]]);
    }

    for (unsigned int i = 0; i < indices.size() / 3; ++i) {
        std::getline(iss, line);
        std::vector<std::string> split = RR::Split(line, ' ');

        if (split[0] != "3")
            throw std::string("Mesh " + mesh + " is not triangulated");

        indices[3 * i] = std::stoi(split[1]);
        indices[3 * i + 1] = std::stoi(split[2]);
        indices[3 * i + 2] = std::stoi(split[3]);
    }

    std::ofstream ofs(FileLoader::AsCachedMeshPath(mesh), std::ofstream::binary);

    unsigned int vCount = vertices.size();
    unsigned int iCount = indices.size();

    ofs.write(reinterpret_cast<char*>(&vCount), sizeof(unsigned int));
    ofs.write(reinterpret_cast<char*>(&iCount), sizeof(unsigned int));

    ofs.write(reinterpret_cast<char*>(vertices.data()), sizeof(Vertex) * vCount);
    ofs.write(reinterpret_cast<char*>(indices.data()), sizeof(unsigned int) * iCount);

    ofs.close();
}

}  // namespace R3D
