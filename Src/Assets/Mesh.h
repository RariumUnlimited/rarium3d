// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_ASSETS_MESH_H_

#define SRC_ASSETS_MESH_H_

#include <string>
#include <vector>

#include <glm/glm.hpp>

#include "Assets/Texture.h"

namespace R3D {

/**
 *    A 3D Mesh
 */
class Mesh {
 public:
    /// A vertex
    struct Vertex {
        glm::vec3 Position;  ///< Position in space
        glm::vec2 TexCoord;  ///< Texture coordinate
        glm::vec3 Normal;  ///< Normal
    };

    /// A material
    struct Material {
        float Ns;  ///< Specular exponent
        glm::vec3 Ka;  ///< Ambient color
        glm::vec3 Ks;  ///< Specular color
        Texture* KdMap;  ///< Diffuse texture
    };

    /**
     * @brief Load a mesh from disk
     * @param mesh Name of the mesh
     */
    explicit Mesh(const std::string& mesh);

    /// Get the name of the mesh
    inline const std::string& GetName() { return mesh; }
    /// Get the vertices of the mesh
    inline const std::vector<Vertex>& GetVertices() { return vertices; }
    /// Get the indices of the mesh
    inline const std::vector<unsigned int>& GetIndices() { return indices; }
    /// Get the material of the mesh
    inline const Material& GetMaterial() { return material; }

 protected:
    /// Load the mesh from a mesh cache file
    void LoadFromCache();
    /// Load the mesh from a ply file
    void LoadFromPly();
    /// Load the material from a mat file
    void LoadMaterial();

    std::string mesh;  ///< Name of the mesh

    std::vector<Vertex> vertices;  ///< Vertices of the mesh
    std::vector<unsigned int> indices;  ///< Indices of the mesh
    Material material;  ///< Material of the mesh
};

}  // namespace R3D

#endif  // SRC_ASSETS_MESH_H_
