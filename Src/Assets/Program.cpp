// Copyright RariumUnlimited - Licence : MIT
#include "Assets/Program.h"

#include <string>
#include <utility>
#include <vector>

#include <Rarium/Tools/Log.h>

#include "Render/GLUtil.h"

namespace R3D {

Program::Program(Shader* vertexShader, Shader* fragmentShader) {
    vertex = vertexShader;
    fragment = fragmentShader;

    gl::GLint compileStatus = 0;
    gl::GLint logSize = 0;
    gl::GLsizei length = 0;

    program = gl::glCreateProgram();
    gl::glAttachShader(program, vertex->GetShader());
    gl::glAttachShader(program, fragment->GetShader());
    gl::glLinkProgram(program);


    gl::glGetProgramiv(program, gl::GL_LINK_STATUS, &compileStatus);

    if (compileStatus != (gl::GLint)gl::GL_TRUE) {
        gl::glGetProgramiv(program, gl::GL_INFO_LOG_LENGTH, &logSize);

        std::vector<gl::GLchar> logAr(logSize);
        gl::glGetProgramInfoLog(program, logSize, &length, logAr.data());
        RR::Logger::Log() << "Shaders " << vertex->GetName() <<
            " + " << fragment->GetName() << " link log : \n" << logAr.data() << "\n";

        throw std::string("Shaders " + vertex->GetName() +
                          " + " + fragment->GetName() +
                          " did not link, see log above");
    }
}

Program::~Program() {
    gl::glDeleteProgram(program);
}

void Program::ExtractData() {
    RR::Logger::Log() << "Shaders " << vertex->GetName() << " + " <<
                         fragment->GetName() << "\n";
    ExtractAttributeData();
    ExtractStorageBlockData();
    ExtractUniformData();
}

void Program::ExtractUniformData() {
    RR::Logger::Log() << "\tUniform :\n";
    char tempNameAR[1024];
    gl::GLint numUni = 0;
    gl::glGetProgramInterfaceiv(program, gl::GL_UNIFORM, gl::GL_ACTIVE_RESOURCES, &numUni);
    const gl::GLenum propertiesAR[3] = { gl::GL_TYPE, gl::GL_LOCATION, gl::GL_BLOCK_INDEX };

    for (int uni = 0; uni < numUni; ++uni) {
        gl::GLint valuesAR[3];
        gl::glGetProgramResourceiv(program, gl::GL_UNIFORM, uni, 3,
                                   propertiesAR, 2, NULL, valuesAR);

        gl::glGetProgramResourceName(program, gl::GL_UNIFORM, uni, 1024,
                                     NULL, tempNameAR);

        std::string uniName(tempNameAR);

        uniforms.insert(
            std::make_pair(uniName,
                           AttributeInfo(uniName,
                                         gl::GLenum(valuesAR[0]),
                                         valuesAR[1],
                                         GLUtil::SizeofGLSLType(gl::GLenum(valuesAR[0])))));

        RR::Logger::Log() << "\t\t" << uniName << " " << valuesAR[1] <<
            " " << GLUtil::GLSLTypeToStr(gl::GLenum(valuesAR[0])) << "\n";
    }
}

void Program::ExtractStorageBlockData() {
    gl::GLint numBlocks = 0;
    gl::glGetProgramInterfaceiv(program,
                                gl::GL_SHADER_STORAGE_BLOCK,
                                gl::GL_ACTIVE_RESOURCES,
                                &numBlocks);

    char tempNameAR[1024];

    RR::Logger::Log() << "\tBuffer :\n";

    for (int blockIdx = 0; blockIdx < numBlocks; ++blockIdx) {
        std::vector<BufferVariableInfo> vars;

        gl::glGetProgramResourceName(program, gl::GL_SHADER_STORAGE_BLOCK,
                                     blockIdx, 1024, NULL, tempNameAR);

        std::string bufferName(tempNameAR);

        gl::GLint binding = 0;
        {
            gl::GLenum prop[] = { gl::GL_BUFFER_BINDING };
            gl::glGetProgramResourceiv(program, gl::GL_SHADER_STORAGE_BLOCK, blockIdx,
                                       1, prop, 1, NULL, &binding);
        }

        RR::Logger::Log() << "\t\t" << bufferName << " index " <<
             blockIdx << " binding " << binding << "\n";

        gl::GLint vcount = 0;
        {
            gl::GLenum prop[] = { gl::GL_NUM_ACTIVE_VARIABLES };
            gl::glGetProgramResourceiv(program, gl::GL_SHADER_STORAGE_BLOCK, blockIdx,
                                       1, prop, 1, NULL, &vcount);
        }

        std::vector<gl::GLint> variables(vcount);
        {
            gl::GLenum prop[] = { gl::GL_ACTIVE_VARIABLES };
            gl::glGetProgramResourceiv(program, gl::GL_SHADER_STORAGE_BLOCK, blockIdx,
                                       1, prop, vcount, NULL, &variables.front());
        }

        gl::GLchar vnameAR[128] = { 0 };
        for (int k = 0; k < vcount; ++k) {
            gl::GLenum propsAR[] = { gl::GL_OFFSET, gl::GL_TYPE, gl::GL_ARRAY_STRIDE,
                                 gl::GL_MATRIX_STRIDE, gl::GL_IS_ROW_MAJOR,
                                 gl::GL_TOP_LEVEL_ARRAY_STRIDE };
            gl::GLint paramsAR[sizeof(propsAR) / sizeof(gl::GLenum)];

            gl::glGetProgramResourceiv(program, gl::GL_BUFFER_VARIABLE, variables[k],
                                       sizeof(propsAR) / sizeof(gl::GLenum), propsAR,
                                       sizeof(paramsAR) / sizeof(gl::GLenum),
                                       NULL, paramsAR);
            gl::glGetProgramResourceName(program, gl::GL_BUFFER_VARIABLE, variables[k],
                                         sizeof(vnameAR), NULL, vnameAR);

            RR::Logger::Log() << "\t\t\t" << std::string(vnameAR) <<
                                 " offset " << paramsAR[0] << " type " <<
                GLUtil::GLSLTypeToStr(gl::GLenum(paramsAR[1])) <<
                " array stride " << paramsAR[2] <<
                " top level stride " << paramsAR[5] << "\n";

            vars.push_back(BufferVariableInfo(std::string(vnameAR),
                                              paramsAR[0],
                                              gl::GLenum(paramsAR[1]),
                                              paramsAR[2], paramsAR[3],
                                              gl::GLboolean(paramsAR[4]),
                                              paramsAR[5]));
        }

        buffers.insert(std::make_pair(bufferName,
                                      BufferInfo(bufferName, blockIdx, vars)));
    }
}

void Program::ExtractAttributeData() {
    RR::Logger::Log() << "\tInput :\n";
    char tempNameAR[1024];

    {
        gl::GLint numInput = 0;
        gl::glGetProgramInterfaceiv(program, gl::GL_PROGRAM_INPUT,
                                    gl::GL_ACTIVE_RESOURCES, &numInput);
        const gl::GLenum propertiesAR[2] = { gl::GL_TYPE, gl::GL_LOCATION };

        for (int input = 0; input < numInput; ++input) {
            gl::GLint valuesAR[2];
            gl::glGetProgramResourceiv(program, gl::GL_PROGRAM_INPUT,
                                       input, 2, propertiesAR, 2, NULL, valuesAR);

            gl::glGetProgramResourceName(program, gl::GL_PROGRAM_INPUT,
                                         input, 1024, NULL, tempNameAR);

            std::string inputName(tempNameAR);

            if (inputName.size() >= 3 && inputName.substr(0, 3) == "gl_") {
                continue;
            } else {
                inputs.insert(
                    std::make_pair(inputName,
                                   AttributeInfo(inputName,
                                                 gl::GLenum(valuesAR[0]),
                                                 valuesAR[1],
                                                 GLUtil::SizeofGLSLType(gl::GLenum(valuesAR[0])))));
                RR::Logger::Log() << "\t\t" << inputName << " " << valuesAR[1] <<
                    " " << GLUtil::GLSLTypeToStr(gl::GLenum(valuesAR[0])) << "\n";
            }
        }
    }

    RR::Logger::Log() << "\tOutput :\n";
    {
        gl::GLint numOutput = 0;
        gl::glGetProgramInterfaceiv(program, gl::GL_PROGRAM_OUTPUT,
                                    gl::GL_ACTIVE_RESOURCES, &numOutput);
        const gl::GLenum propertiesAR[2] = { gl::GL_TYPE, gl::GL_LOCATION };

        for (int outpout = 0; outpout < numOutput; ++outpout) {
            gl::GLint valuesAR[2];
            gl::glGetProgramResourceiv(program, gl::GL_PROGRAM_OUTPUT, outpout,
                                       2, propertiesAR, 2, NULL, valuesAR);

            gl::glGetProgramResourceName(program, gl::GL_PROGRAM_OUTPUT, outpout,
                                         1024, NULL, tempNameAR);

            std::string outputName(tempNameAR);

            if (outputName.size() >= 3 && outputName.substr(0, 3) != "gl_") {
                outputs.insert(
                    std::make_pair(outputName,
                                   AttributeInfo(outputName,
                                                 gl::GLenum(valuesAR[0]),
                                                 valuesAR[1],
                                                 GLUtil::SizeofGLSLType(gl::GLenum(valuesAR[0])))));
                RR::Logger::Log() << "\t\t" + outputName << " " <<
                    valuesAR[1] << " " <<
                    GLUtil::GLSLTypeToStr(gl::GLenum(valuesAR[0])) << "\n";
            }
        }
    }
}

}  // namespace R3D
