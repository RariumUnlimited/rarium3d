// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_ASSETS_PROGRAM_H_

#define SRC_ASSETS_PROGRAM_H_

#include <map>
#include <string>
#include <vector>

#include "Assets/Shader.h"

namespace R3D {

/**
 *    Information about a shader attribute
 */
struct AttributeInfo {
    std::string Name;  ///< Name of the attribute
    gl::GLenum Type;  ///< Type of the attribute
    gl::GLsizei Size;  ///< Size in byte of the attribute
    gl::GLint Location;  ///< Location of the attribute

    /// Create an attribute info
    AttributeInfo(std::string name,
                  gl::GLenum type,
                  gl::GLint location,
                  gl::GLsizei size) {
        Name = name;
        Type = type;
        Location = location;
        Size = size;
    }

    AttributeInfo() { }
};

/**
 *    Information about a variable in a shader buffer
 */
struct BufferVariableInfo {
    std::string Name;  ///< Name of the variable
    gl::GLint Offset;  ///< Offset of the variable
    gl::GLenum Type;  ///< Type of the variable
    gl::GLint ArrayStride;  ///< Array Stride of the variable
    gl::GLint MatrixStride;  ///< Matrix Stride of the variable
    gl::GLboolean IsRowMajor;  ///< If the matrix is in row major
    gl::GLint TopLevelArrayStide;  ///< Top level array stride of the variable

    /// Create a new buffer variable info
    BufferVariableInfo(std::string name,
                       gl::GLint offset,
                       gl::GLenum type,
                       gl::GLint arrayStride,
                       gl::GLint matrixStride,
                       gl::GLboolean isRowMajor,
                       gl::GLint topLevelArrayStide) {
        Name = name;
        Offset = offset;
        Type = type;
        ArrayStride = arrayStride;
        MatrixStride = matrixStride;
        IsRowMajor = isRowMajor;
        TopLevelArrayStide = topLevelArrayStide;
    }

    BufferVariableInfo() { }
};

/**
 *    Information about a buffer in a shader
 */
struct BufferInfo {
    std::string Name;  ///< Name of the buffer
    gl::GLint Index;  ///< Index of the buffer
    std::vector<BufferVariableInfo> Variables;  ///< List of all variable of the buffer

    /// Create a new buffer info
    BufferInfo(std::string name,
               gl::GLint index,
               std::vector<BufferVariableInfo> variables) {
        Name = name;
        Index = index;
        Variables = variables;
    }

    BufferInfo() { }
};

/**
 * An OpenGL program pipeline
 */
class Program {
 public:
    /**
     *    Create a new OpenGL program
     *    @param vertexShader Vertex shader of the program
     *    @param fragmentShader Fragment shader of the program
     */
    Program(Shader* vertexShader, Shader* fragmentShader);
    /**
     *    Destroy the program
     */
    ~Program();

    /// Get the OpenGL name of the program
    inline gl::GLuint GetProgram() { return program; }

    /// Get the input of this shader
    inline std::map<std::string, AttributeInfo>& GetInputs() {
        return inputs;
    }
    /// Get the output of this shader
    inline std::map<std::string, AttributeInfo>& GetOutputs() {
        return outputs;
    }
    /// Get the uniform of this shader
    inline std::map<std::string, AttributeInfo>& GetUniforms() {
        return uniforms;
    }
    /// Get the buffer used by this shader
    inline std::map<std::string, BufferInfo>& GetBuffers() {
        return buffers;
    }

    /// Check if a given input is active in the program
    inline bool IsInputActive(std::string input) {
        return inputs.find(input) != inputs.cend();
    }
    /// Check if a given output is active in the program
    inline bool IsOutputActive(std::string output) {
        return outputs.find(output) != outputs.cend();
    }
    /// Check if a uniform is active in the program
    inline bool IsUniformActive(std::string uniform) {
        return uniforms.find(uniform) != uniforms.cend();
    }
    /// Check if a buffer is active in the program
    inline bool IsBufferActive(std::string buffer) {
        return buffers.find(buffer) != buffers.cend();
    }

    /**
     *    Extract data (such as uniform, input,...) from the shader
     */
    void ExtractData();

 protected:
    Shader* vertex;  ///< Vertex shader of the program
    Shader* fragment;  ///< Fragment shader of the program
    gl::GLuint program;  ///< OpenGL name of the program

    std::map<std::string, AttributeInfo> inputs;  ///< Input of the shader
    std::map<std::string, AttributeInfo> outputs;  ///< Output of the shader
    std::map<std::string, AttributeInfo> uniforms;  ///< Uniform of the shader
    std::map<std::string, BufferInfo> buffers;  ///< Buffer used by the shader

    /**
     *    Extract the attribute of the program
     */
    void ExtractAttributeData();
    /**
     *    Extract Shader Storage Buffer data from the shader
     */
    void ExtractStorageBlockData();
    /**
     *    Extract Attribute data from the shader
     */
    void ExtractUniformData();
};

}  // namespace R3D

#endif  // SRC_ASSETS_PROGRAM_H_
