// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_ASSETS_SCENE_CAMERA_H_

#define SRC_ASSETS_SCENE_CAMERA_H_

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <Rarium/Window/InputState.h>

namespace R3D {

/// Represent a virtual camera of a scene
class Camera {
 public:
    /**
     *	Build a new camera
     *	@param fieldOfView Field of view of the camera
     *	@param nearPlaneDistance Distance to the near view plane
     *	@param farPlaneDistance Ditance to the far view plane
     *	@param viewport Viewport of the camera
     */
    Camera(float fieldOfView,
           float nearPlaneDistance,
           float farPlaneDistance,
           glm::uvec2 viewport) :
        fieldOfView(fieldOfView),
        nearPlaneDistance(nearPlaneDistance),
        farPlaneDistance(farPlaneDistance),
        pos(0.f, 0.f, -1.f),
        lookAt(0.f, 0.f, 0.f),
        up(0.f, 1.f, 0.f),
        viewport(viewport),
        projectionMatrix(glm::perspective(glm::radians(GetFieldOfView()),
                                          static_cast<float>(GetViewport().x) /
                                          static_cast<float>(GetViewport().y),
                                          GetNearPlaneDistance(),
                                          GetFarPlaneDistance())) {
    }

    /**
     * @brief Cmaera virtual destructor
     */
    virtual ~Camera() {}

    /**
     *	Update the camera position and lookAt
     *	@param delta Seconds since last update
     *	@param inputState State of user input and action
     */
    virtual void Update(float delta,
                        const RR::InputState& inputState,
                        const RR::InputState& lastInputState) {
        viewMatrix = glm::lookAt(GetPosition(), GetLookAt(), GetUp());
    }

    /// Get the position of the camera
    inline glm::vec3 GetPosition() { return pos; }
    /// Get where the camera look
    inline glm::vec3 GetLookAt() { return lookAt; }
    /// Get the up vector for the camera
    inline glm::vec3 GetUp() { return up; }
    /// Get the viewport of the camera
    inline glm::uvec2 GetViewport() { return viewport; }
    /// Set the position of the camera
    inline void SetPosition(glm::vec3 position) { this->pos = position; }
    /// Set where the camera must look
    inline void SetLookAt(glm::vec3 lookAt) { this->lookAt = lookAt; }
    /// Set the up vector of the camera
    inline void SetUp(glm::vec3 up) { this->up = up; }
    /// Get the field of view of this camera
    inline float GetFieldOfView() { return fieldOfView; }
    /// Get the distance to the near plane of the camera
    inline float GetNearPlaneDistance() { return nearPlaneDistance; }
    /// Get the distance to the far plane of the camera
    inline float GetFarPlaneDistance() { return farPlaneDistance; }
    /// Get the projection matrix of the camera
    inline glm::mat4 GetProjectionMatrix() { return projectionMatrix; }
    /// Get the view matrix of the camera
    inline glm::mat4 GetViewMatrix() { return viewMatrix; }


 protected:
    float fieldOfView;  ///< Field of view of the camera in degree
    float nearPlaneDistance;  ///< Distance to the near plane of the camera
    float farPlaneDistance;  ///< Distance to the far plane of the camera
    glm::vec3 pos;  ///< Position of the camera
    glm::vec3 lookAt;  ///< Where the camera look
    glm::vec3 up;  ///< The up vector of the camera
    glm::uvec2 viewport;  ///< Viewport of the camera
    glm::mat4 viewMatrix;  ///< View matrix of the camera
    glm::mat4 projectionMatrix;  ///< Projection matrix of the camera
};

}  // namespace R3D

#endif  // SRC_ASSETS_SCENE_CAMERA_H_
