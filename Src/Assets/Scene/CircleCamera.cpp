// Copyright RariumUnlimited - Licence : MIT
#include "Assets/Scene/CircleCamera.h"

#include <cmath>

namespace R3D {

CircleCamera::CircleCamera(float fieldOfView,
                           float nearPlaneDistance,
                           float farPlaneDistance,
                           glm::uvec2 viewport,
                           glm::vec2 lookat,
                           float r,
                           float h,
                           float speed) :
    Camera(fieldOfView, nearPlaneDistance, farPlaneDistance, viewport),
    speed(speed),
    r(r),
    h(h),
    currentAngle(0.f) {


    lookAt = glm::vec3(lookat.x, h, lookat.y);
    pos = lookAt + glm::vec3(0.f, 0.f, r);
    up = glm::vec3(0.f, 1.f, 0.f);

    RR::InputState isTemp;
    Update(0, isTemp, isTemp);
}

void CircleCamera::Update(float delta,
                          const RR::InputState& inputState,
                          const RR::InputState& lastInputState) {
    currentAngle += speed * delta;
    this->pos.x = r * std::cos(currentAngle);
    this->pos.z = r * std::sin(currentAngle);

    currentAngle = currentAngle > 2.f * 3.14f ? currentAngle - 2.f * 3.14f : currentAngle;

    Camera::Update(delta, inputState, lastInputState);
}

}  // namespace R3D
