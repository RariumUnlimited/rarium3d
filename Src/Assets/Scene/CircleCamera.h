// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_ASSETS_SCENE_CIRCLECAMERA_H_

#define SRC_ASSETS_SCENE_CIRCLECAMERA_H_

#include "Assets/Scene/Camera.h"

namespace R3D  {

/// Represent a virtual camera that circle around a point
class CircleCamera : public Camera {
 public:
    /**
     *    Build a new camera
     *    @param fieldOfView Field of view of the camera
     *    @param nearPlaneDistance Distance to the near view plane
     *    @param farPlaneDistance Ditance to the far view plane
     */
    CircleCamera(float fieldOfView,
                 float nearPlaneDistance,
                 float farPlaneDistance,
                 glm::uvec2 viewport,
                 glm::vec2 lookat,
                 float r,
                 float h,
                 float speed);

    void Update(float delta,
                const RR::InputState& inputState,
                const RR::InputState& lastInputState) override;

 protected:
    float currentAngle;  ///< Current angle of the camera in rad
    float speed;  ///< Speed of the camera in rad/s
    float r;  ///< Radius of the circle
    float h;  ///< At which height the camera is circling
};

}  // namespace R3D

#endif  // SRC_ASSETS_SCENE_CIRCLECAMERA_H_
