// Copyright RariumUnlimited - Licence : MIT
#include "Assets/Scene/FixedCamera.h"

namespace R3D {

FixedCamera::FixedCamera(float fieldOfView,
                         float nearPlaneDistance,
                         float farPlaneDistance,
                         glm::uvec2 viewport,
                         glm::vec3 pos,
                         glm::vec3 lookat,
                         glm::vec3 up) :
    Camera(fieldOfView, nearPlaneDistance, farPlaneDistance, viewport) {
    this->pos = pos;
    this->lookAt = lookat;
    this->up = up;
    RR::InputState isTemp;
    Update(0, isTemp, isTemp);
}

}  // namespace R3D
