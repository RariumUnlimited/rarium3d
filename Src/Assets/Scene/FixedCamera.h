// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_ASSETS_SCENE_FIXEDCAMERA_H_

#define SRC_ASSETS_SCENE_FIXEDCAMERA_H_

#include "Assets/Scene/Camera.h"

namespace R3D  {

/// Represent a virtual freefly camera of a scene
class FixedCamera : public Camera {
 public:
    /**
     *  Build a new camera
     *  @param fieldOfView Field of view of the camera
     *  @param nearPlaneDistance Distance to the near view plane
     *  @param farPlaneDistance Ditance to the far view plane
     *  @param viewport Viewport of the camera
     */
    FixedCamera(float fieldOfView,
                float nearPlaneDistance,
                float farPlaneDistance,
                glm::uvec2 viewport,
                glm::vec3 pos,
                glm::vec3 lookat,
                glm::vec3 up);
};

}  // namespace R3D

#endif  // SRC_ASSETS_SCENE_FIXEDCAMERA_H_
