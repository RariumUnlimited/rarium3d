// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_ASSETS_SCENE_MESHINSTANCE_H_

#define SRC_ASSETS_SCENE_MESHINSTANCE_H_

#include <string>

#include <glm/glm.hpp>

#include "Assets/Instance.h"
#include "Assets/Mesh.h"

namespace R3D {

/**
 * An instance of an texture
 */
struct MeshInstance : public Instance {
    explicit MeshInstance(Mesh* mesh) :
        Instance(T::Mesh),
        ModelMatrix(std::get<MeshData>(Data).ModelMatrix) {
        Technique = "Mesh";
        std::get<MeshData>(Data).Mesh = mesh;
        std::get<MeshData>(Data).ModelMatrix = glm::mat4(1.f);
    }

    glm::mat4& ModelMatrix;
};

}  // namespace R3D

#endif  // SRC_ASSETS_SCENE_MESHINSTANCE_H_
