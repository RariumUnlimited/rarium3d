// Copyright RariumUnlimited - Licence : MIT
#include "Assets/Scene/Scene.h"

namespace R3D {

Scene::Scene(Camera *camera, Ui* ui) :
    camera(camera),
    ui(ui) {
    lightData.Ambient = glm::vec4(0.1f, 0.1f, 0.1f, 1.f);
    lightData.Power = glm::vec4(1.f, 1.f, 1.f, 1.f);
    lightData.Position = glm::vec4(0.f, 0.f, 5.f, 1.f);
    lightData.Direction = glm::vec4(0.f, 0.f, -1.f, glm::radians(30.f));

    lightData.VPMatrix = glm::perspective(lightData.Direction.w,
                                          1.f,
                                          0.1f,
                                          100.f) *
                         glm::lookAt(glm::vec3(lightData.Position),
                                     glm::vec3(lightData.Position) +
                                     glm::vec3(lightData.Direction),
                                     glm::vec3(0.f, 1.f, 0.f));
}

void Scene::Update(float delta,
                           const RR::InputState& state,
                           const RR::InputState& lastState) {
    camera->Update(delta, state, lastState);
}

}  // namespace R3D
