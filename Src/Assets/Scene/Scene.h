// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_ASSETS_SCENE_SCENE_H_

#define SRC_ASSETS_SCENE_SCENE_H_

#include <memory>
#include <string>
#include <vector>

#include "Assets/Scene/Camera.h"
#include "Assets/Scene/Ui.h"
#include "Render/Data/LightData.h"
#include "Render/Data/Pass.h"

namespace R3D {

/**
 *	A scene in the application
 */
class Scene {
 public:
    /// Scene constructor
    Scene(Camera *camera, Ui* ui);

    /**
     *	Update the scene
     *	@param delta Delta time since last update
     *	@param state State of user input in current frame
     *	@param lastState State of user input in last frame
     */
    virtual void Update(float delta,
                        const RR::InputState& state,
                        const RR::InputState& lastState);

    /// Return the camera of the scene
    inline Camera* GetCamera() { return camera; }
    /// Return the ui of the scene
    inline Ui* GetUi() { return ui; }
    /// Get all instances of the scene
    inline std::vector<Instance*> GetInstances() { return instances; }
    /// Get the light data of the scene
    inline LightData GetLightData() { return lightData; }
    /// Get all additional render passes of the scene
    inline std::vector<Pass*> GetPasses() { return passes; }

 protected:
    Camera* camera;  ///< Camera used to view the 3D scene
    Ui* ui;  ///< User interface for the scene
    std::vector<Instance*> instances;  ///< Instances of the scene
    LightData lightData;  ///< Light data of the scene
    std::vector<Pass*> passes;  ///< Additional render passes of the scene
};

/**
 *	Describe a scene, this is used to allocate a new scene
 */
class SceneDescriptor {
 public:
    explicit SceneDescriptor(std::string name) : name(name) { }
    /**
     *	Return a new Scene corresponding to the descriptor
     */
    virtual std::unique_ptr<Scene> GetScene() = 0;
    /// Get the name of the scene
    inline std::string GetName() { return name; }
 protected:
    std::string name;  ///< Name of the scene
};

}  // namespace R3D

#endif  // SRC_ASSETS_SCENE_SCENE_H_
