// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_ASSETS_SCENE_TEXTUREINSTANCE_H_

#define SRC_ASSETS_SCENE_TEXTUREINSTANCE_H_

#include <string>

#include <glm/glm.hpp>

#include "Assets/Instance.h"

namespace R3D {

/**
 * An instance of an texture
 */
struct TextureInstance : public Instance {
    TextureInstance(Texture* tex, glm::vec2 position) :
        Instance(T::Text) {
        Technique = "Texture";
        TexData& td = std::get<TexData>(Data);
        td.Tex = tex;
        td.Position = position;
        td.Z = 0.1f;
        td.Size = glm::vec2(tex->GetWidth(), tex->GetHeight());
    }
};

}  // namespace R3D

#endif  // SRC_ASSETS_SCENE_TEXTUREINSTANCE_H_
