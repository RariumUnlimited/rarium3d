// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_UI_UI_H_

#define SRC_UI_UI_H_

#include <vector>

#include <Rarium/Ui/Ui.h>
#include <SFML/Graphics.hpp>

#include "Window/WindowFunction.h"

namespace R3D {

/**
 *    A User Interface to display on screen
 */
class Ui : public RR::Ui {
 public:
    /// Set the object that can be used to call ui functions
    inline void SetUiFunction(WindowFunction* windowFunction) {
        this->windowFunction = windowFunction;
    }
    /// Get the pointer to the object that can be used to call ui functions
    inline WindowFunction* GetUiFunction() {
        return windowFunction;
    }

 protected:
    WindowFunction* windowFunction;  ///< Pointer to the object that can be used to call ui functions
};

}  // namespace Rarium

#endif  // SRC_UI_UI_H_
