﻿  // Copyright RariumUnlimited - Licence : MIT
#include "Assets/Shader.h"

#include <map>
#include <sstream>

#include <glm/glm.hpp>
#include <Rarium/Tools/FileLoader.h>
#include <Rarium/Tools/Log.h>

#include "Assets/FileLoader.h"
#include "Render/Renderer.h"
#include "Render/GLUtil.h"

namespace R3D {

Shader::Shader(std::string name) :
    name(name) {
    if (name.find(".vs") != std::string::npos)
        type = gl::GL_VERTEX_SHADER;
    else if (name.find(".fs") != std::string::npos)
        type = gl::GL_FRAGMENT_SHADER;
    else if (name.find(".gs") != std::string::npos)
        type = gl::GL_GEOMETRY_SHADER;
    else if (name.find(".cs") != std::string::npos)
        type = gl::GL_COMPUTE_SHADER;
    else if (name.find(".tcs") != std::string::npos)
        type = gl::GL_TESS_CONTROL_SHADER;
    else if (name.find(".tes") != std::string::npos)
        type = gl::GL_TESS_EVALUATION_SHADER;
    else
        throw std::string("No shader type found");

    std::string src = RR::LoadFileAsString(FileLoader::AsShaderPath(name), "");

    gl::GLuint tempShader = gl::glCreateShader(type);

    const char* tempSrcAR = src.c_str();

    gl::glShaderSource(tempShader, 1, &tempSrcAR, nullptr);
    gl::glCompileShader(tempShader);

    gl::GLint compileStatus = 0;
    gl::GLint logSize = 0;
    gl::GLsizei length = 0;
    gl::GLchar* logAR;

    gl::glGetShaderiv(tempShader, gl::GL_COMPILE_STATUS, &compileStatus);

    if (compileStatus != (gl::GLint)gl::GL_TRUE) {
        gl::glGetShaderiv(tempShader, gl::GL_SHADER_SOURCE_LENGTH, &logSize);
        std::vector<gl::GLchar> logAR(logSize);
        gl::glGetShaderSource(tempShader, logSize, &length, logAR.data());
        RR::Logger::Log() << "Shader " << name << " source :\n" << logAR.data() << "\n";

        gl::glGetShaderiv(tempShader, gl::GL_INFO_LOG_LENGTH, &logSize);
        std::fill(logAR.begin(), logAR.end(), 0);
        gl::glGetShaderInfoLog(tempShader, logSize, &length, logAR.data());
        RR::Logger::Log() << "Shader " << name << " compile log : " << logAR.data();

        throw std::string("Shader " + name + " did not compile , see log above");
    }

    shader = tempShader;

    CheckGLError();
}

Shader::~Shader() {
    gl::glDeleteShader(shader);
}

}  // namespace R3D
