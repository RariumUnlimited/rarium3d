// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_ASSETS_SHADER_H_

#define SRC_ASSETS_SHADER_H_

#include <glbinding/gl/gl.h>

#include <string>

namespace R3D {

/**
 *    An OpenGL Shader
 */
class Shader {
 public:
    /**
     *    Create a new shader on the gpu
     *    @param name Name of the shader
     *    @param src Source of the shader
     */
    explicit Shader(std::string name);
    /**
     *    Destroy our shader
     */
    ~Shader();

    /// Get the separate program of this shader
    inline gl::GLuint GetShader() { return shader; }
    /// Get the type of this shader
    inline gl::GLenum GetType() { return type; }
    /// Get the name of the shader
    inline std::string GetName() { return name; }

 protected:
    std::string name;  ///< Name of the shader
    gl::GLuint shader;  ///< Separate program of the shader on the GPU
    gl::GLenum type;  ///< Type of the shader
};

}  // namespace R3D

#endif  // SRC_ASSETS_SHADER_H_
