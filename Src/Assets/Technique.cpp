// Copyright RariumUnlimited - Licence : MIT
#include "Assets/Technique.h"

#include "Assets/Collection.h"

namespace R3D {

Technique::Technique(std::string name) :
    name(name) {
    program = Collection::Programs()[name];
}

}  // namespace R3D
