// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_ASSETS_TECHNIQUE_H_

#define SRC_ASSETS_TECHNIQUE_H_

#include <string>

#include "Assets/Instance.h"
#include "Assets/Program.h"

namespace R3D {

/**
 * A technique that is used to draw things on screen
 */
class Technique {
 public:
    /**
     * Build a new technique
     * @param name Name of the technique
     */
    explicit Technique(std::string name);

    /**
     * Get the type of instance the technique is used to draw
     * @return The type to which instance that technique applies
     */
    virtual Instance::T GetType() = 0;
    /**
     * Get the name of the technique
     * @return The name of the technique
     */
    inline std::string GetName() { return name; }
    /**
     * Get the gpu program associated with this technique
     * @return The gpu program associated with this technique
     */
    inline Program* GetProgram() { return program; }

 protected:
    Program* program;  ///< Gpu program associated with this technique
    std::string name;  ///< Name of the technique
};

}  // namespace R3D

#endif  // SRC_ASSETS_TECHNIQUE_H_
