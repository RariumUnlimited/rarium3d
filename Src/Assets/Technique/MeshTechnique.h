// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_ASSETS_TECHNIQUE_MESHTECHNIQUE_H_

#define SRC_ASSETS_TECHNIQUE_MESHTECHNIQUE_H_

#include <string>

#include "Assets/Technique.h"

namespace R3D {

class MeshTechnique : public Technique {
 public:
    explicit MeshTechnique(std::string pName = "Mesh") : Technique(pName) { }

    inline Instance::T GetType() override { return Instance::T::Mesh; }
};

}  // namespace R3D

#endif  // SRC_ASSETS_TECHNIQUE_MESHTECHNIQUE_H_

