// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_ASSETS_TECHNIQUE_SHADOWTECHNIQUE_H_

#define SRC_ASSETS_TECHNIQUE_SHADOWTECHNIQUE_H_

#include <string>

#include "Assets/Technique.h"

namespace R3D {

class ShadowTechnique : public Technique {
 public:
    explicit ShadowTechnique(std::string pName = "Shadow") : Technique(pName) { }

    inline Instance::T GetType() override { return Instance::T::Mesh; }
};

}  // namespace R3D

#endif  // SRC_ASSETS_TECHNIQUE_SHADOWTECHNIQUE_H_

