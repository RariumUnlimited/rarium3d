// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_ASSETS_TECHNIQUE_SKYBOXTECHNIQUE_H_

#define SRC_ASSETS_TECHNIQUE_SKYBOXTECHNIQUE_H_

#include <string>

#include "Assets/Technique.h"

namespace R3D {

class SkyboxTechnique : public Technique {
 public:
    explicit SkyboxTechnique(std::string pName = "Skybox") : Technique(pName) { }

    inline Instance::T GetType() override { return Instance::T::Mesh; }
};

}  // namespace R3D

#endif  // SRC_ASSETS_TECHNIQUE_SKYBOXTECHNIQUE_H_

