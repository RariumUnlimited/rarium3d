// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_ASSETS_TECHNIQUE_TEXTUREGREENTECHNIQUE_H_

#define SRC_ASSETS_TECHNIQUE_TEXTUREGREENTECHNIQUE_H_

#include <string>

#include "Assets/Technique/TextureTechnique.h"

namespace R3D {

class TextureGreenTechnique : public TextureTechnique {
 public:
    explicit TextureGreenTechnique(std::string name = "TextureGreen") :
            TextureTechnique(name) { }

    inline Instance::T GetType() override { return Instance::T::Text; }
};

}  // namespace R3D

#endif  // SRC_ASSETS_TECHNIQUE_TEXTUREGREENTECHNIQUE_H_

