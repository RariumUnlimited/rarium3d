// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_ASSETS_TECHNIQUE_TEXTURETECHNIQUE_H_

#define SRC_ASSETS_TECHNIQUE_TEXTURETECHNIQUE_H_

#include <string>

#include "Assets/Technique.h"

namespace R3D {

class TextureTechnique : public Technique {
 public:
    explicit TextureTechnique(std::string pName = "Texture") : Technique(pName) { }

    inline Instance::T GetType() override { return Instance::T::Text; }
};

}  // namespace R3D

#endif  // SRC_ASSETS_TECHNIQUE_TEXTURETECHNIQUE_H_

