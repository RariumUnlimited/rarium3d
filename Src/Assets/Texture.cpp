// Copyright RariumUnlimited - Licence : MIT
#include "Assets/Texture.h"

#include "Assets/FileLoader.h"

namespace R3D {

Texture::Texture(std::string path, bool flip) :
    path(path) {
    sf::Image image;
    image.loadFromFile(FileLoader::AsTexturePath(path));
    if (flip)
        image.flipVertically();

    if (texture.loadFromImage(image)) {
        name = texture.getNativeHandle();
        handle = gl::glGetTextureHandleARB(name);
        gl::glMakeTextureHandleResidentARB(handle);

        width = texture.getSize().x;
        height = texture.getSize().y;
    } else {
        throw std::string("Unable to load texture : " +
                          std::string(FileLoader::AsTexturePath(path)));
    }
}

}  // namespace R3D
