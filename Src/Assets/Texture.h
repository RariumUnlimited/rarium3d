// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_ASSETS_TEXTURE_H_

#define SRC_ASSETS_TEXTURE_H_

#include <glbinding/gl/gl.h>

#include <string>

#include <glm/glm.hpp>
#include <SFML/Graphics.hpp>

namespace R3D {

/**
 *    An OpenGL texture
 */
class Texture {
 public:
    /**
     *    Create a new texture from a file
     *    @param path Path to the texture relative to the assets directory
     *    @param flip If the texture must be flipped
     */
    explicit Texture(std::string path, bool flip = true);

    /// Get the path of the texture
    inline std::string GetPath() { return path; }
    /// Get the OpenGL name of the texture
    inline gl::GLuint GetName() { return name; }
    /// Get the width of the texture
    inline gl::GLuint GetWidth() { return width; }
    /// Get the height of the texture
    inline gl::GLuint GetHeight() { return height; }
    /// Get the handle of the texture on GPU
    inline gl::GLuint64 GetHandle() { return handle; }
    /// Get the underlying SFML texture
    inline const sf::Texture& GetSFTexture() { return texture; }

 public:
    std::string path;  ///< Path to the texture
    sf::Texture texture;  ///< SFML texture
    gl::GLuint name;  ///< OpenGL name of the texture
    gl::GLuint64 handle;  ///< GPU handle of the texture
    gl::GLuint width;  ///< Width of the texture
    gl::GLuint height;  ///< Height of the texture
};

}  // namespace R3D

#endif  // SRC_ASSETS_TEXTURE_H_
