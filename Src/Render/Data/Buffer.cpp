// Copyright RariumUnlimited - Licence : MIT
#include "Render/Data/Buffer.h"

#include <algorithm>
#include <cstring>
#include <string>

#include "Render/Renderer.h"

namespace R3D {

    Buffer::Buffer(gl::GLsizei size, std::string label) :
    size(size),
    label(label) {
        if (size > 0) {
            gl::glCreateBuffers(1, &name);
            gl::glNamedBufferStorage(name, size, nullptr,
                                 gl::GL_MAP_READ_BIT |
                                 gl::GL_MAP_WRITE_BIT |
                                 gl::GL_MAP_PERSISTENT_BIT);
            map = glMapNamedBufferRange(name, 0, size,
                                        gl::GL_MAP_READ_BIT |
                                        gl::GL_MAP_WRITE_BIT |
                                        gl::GL_MAP_PERSISTENT_BIT);

            CheckGLError();
        }
    }

    Buffer::~Buffer() {
        gl::glDeleteBuffers(1, &name);
    }

    void Buffer::Update(void* data, gl::GLsizei dataSize) {
        if (size > 0) {
            // We need to update the correct size of that, our buffer are not dynamic...
            gl::GLsizei sizeToUpdate = std::min(dataSize, size);
            memcpy(map, data, sizeToUpdate);
        }
    }

}  // namespace R3D
