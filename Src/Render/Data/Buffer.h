// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RENDER_DATA_BUFFER_H_

#define SRC_RENDER_DATA_BUFFER_H_

#include <glbinding/gl/gl.h>

#include <string>

namespace R3D {

/**
 *    A static OpenGL buffer
 */
class Buffer {
 public:
    /**
     *    Build a new buffer
     *    @param size size in byte of the buffer
     *    @param label A label for the buffer
     */
    explicit Buffer(gl::GLsizei size, std::string label = "");
    /**
     *    Destroy the buffer
     */
    ~Buffer();
    /**
     *    Update the buffer with new data
     *    @param data Pointer to the new data
     *    @param dataSize Size in byte of the new data
     */
    void Update(void* data, gl::GLsizei dataSize);

    /// Get the OpenGL name of the buffer
    inline gl::GLuint GetName() { return name; }
    /// Get a pointer to the buffer mapped in memory
    inline void* GetMap() { return map; }
    /// Get the label of the buffer
    inline std::string GetLabel() { return label; }
    /// Get the size in byte of the buffer
    inline gl::GLsizei GetSize() { return size; }

    /// Change the label of the buffer
    inline void SetLabel(std::string label) { this->label = label; }

 protected:
    gl::GLuint name;  ///< OpenGL name of the buffer
    void* map;  ///< Point to the buffer mapped in memory
    std::string label;  ///< Label of the buffer
    gl::GLsizei size;  ///< Size in byte of the buffer
};

}  // namespace R3D

#endif  // SRC_RENDER_DATA_BUFFER_H_
