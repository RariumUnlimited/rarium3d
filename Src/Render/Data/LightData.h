// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RENDER_DATA_LIGHTDATA_H_

#define SRC_RENDER_DATA_LIGHTDATA_H_

#include <glbinding/gl/gl.h>

#include <string>

#include <glm/glm.hpp>

namespace R3D {

/**
 *    Structure used to store data about the light of the scene (a spotlight)
 *    glm::vec4 because alignement
 */
struct LightData {
    glm::vec4 Ambient;  ///< Ambient light in the scene
    glm::vec4 Power;  ///< Radiance emitted by the light
    glm::vec4 Position;  ///< Position of the light in the scene
    glm::vec4 Direction;  ///< Direction of the light (xyz), light cutoff (w)
    glm::mat4 VPMatrix;  ///< View Projection matrix of the light
    gl::GLuint64 ShadowMap;  ///< Handle of the shadow map of the light
};

}  // namespace R3D

#endif  // SRC_RENDER_DATA_LIGHTDATA_H_
