// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RENDER_DATA_PASS_H_

#define SRC_RENDER_DATA_PASS_H_

#include <string>

#include "Assets/Instance.h"
#include "Assets/Technique.h"

namespace R3D {

/**
 *    A rendering pass
 */
class Pass {
 public:
    /**
     *    Build a new pass
     *    @param technique technique used for the pass
     */
    explicit Pass(std::string technique) :
        technique(technique) { }

    /**
     *    Pass destructor
     */
    virtual ~Pass() {}

    /// Setup anything that the pass need
    virtual void Setup() {}
    /// Clean up anything that was setup for the pass
    virtual void CleanUp() {}

    /// Get the type of instance to which this pass applies
    virtual Instance::T GetType() = 0;
    /// Get the technique of the pass
    inline std::string GetTechnique() { return technique; }

 protected:
    std::string technique;  ///< Technique used for the pass
};

}  // namespace R3D

#endif  // SRC_RENDER_DATA_PASS_H_
