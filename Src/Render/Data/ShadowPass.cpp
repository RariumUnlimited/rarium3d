// Copyright RariumUnlimited - Licence : MIT
#include "Render/Data/ShadowPass.h"

#include <string>

#include <SFML/Graphics.hpp>

namespace R3D {

ShadowPass::ShadowPass(LightData& lightdata) :
    Pass("Shadow"),
    lightdata(lightdata) {
    gl::glCreateFramebuffers(1, &framebuffer);
    gl::glCreateTextures(gl::GL_TEXTURE_2D, 1, &texture);
    gl::glTextureStorage2D(texture, 1, gl::GL_DEPTH_COMPONENT24, 1024, 1024);
    gl::glTextureParameteri(texture, gl::GL_TEXTURE_MAG_FILTER, gl::GL_NEAREST);
    gl::glTextureParameteri(texture, gl::GL_TEXTURE_MIN_FILTER, gl::GL_NEAREST);
    gl::glTextureParameteri(texture, gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
    gl::glTextureParameteri(texture, gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);

    textureHandle = gl::glGetTextureHandleARB(texture);
    gl::glMakeTextureHandleResidentARB(textureHandle);

    gl::glNamedFramebufferTexture(framebuffer, gl::GL_DEPTH_ATTACHMENT, texture, 0);
    gl::glNamedFramebufferDrawBuffer(framebuffer, gl::GL_NONE);

    if (gl::glCheckNamedFramebufferStatus(framebuffer, gl::GL_FRAMEBUFFER)
            != gl::GL_FRAMEBUFFER_COMPLETE)
        throw std::string("Shadow pass framebuffer not complete.");

    lightdata.ShadowMap = textureHandle;
}

ShadowPass::~ShadowPass() {
    gl::glDeleteFramebuffers(1, &framebuffer);
    gl::glDeleteTextures(1, &texture);
}


void ShadowPass::Setup() {
    gl::glGetIntegerv(gl::GL_VIEWPORT, viewportData);

    gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, framebuffer);

    gl::glClear(gl::GL_DEPTH_BUFFER_BIT);

    gl::glViewport(0, 0, 1024, 1024);

    gl::glCullFace(gl::GL_FRONT);
}

void ShadowPass::CleanUp() {
    gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, 0);

    gl::glViewport(viewportData[0],
                   viewportData[1],
                   viewportData[2],
                   viewportData[3]);

    gl::glCullFace(gl::GL_BACK);
}

}  // namespace R3D
