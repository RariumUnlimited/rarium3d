// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RENDER_DATA_SHADOWPASS_H_

#define SRC_RENDER_DATA_SHADOWPASS_H_

#include <glbinding/gl/gl.h>

#include "Render/Data/LightData.h"
#include "Render/Data/Pass.h"

namespace R3D {

/**
 *    A shadow rendering pass
 */
class ShadowPass : public Pass {
 public:
    /**
     *    Build a new shadow pass
     */
    explicit ShadowPass(LightData& lightdata);

    /**
     *    Pass destructor
     */
    ~ShadowPass() override;

    /// Setup anything that the pass need
    void Setup() override;
    /// Clean up anything that was setup for the pass
    void CleanUp() override;

    inline Instance::T GetType() override { return Instance::T::Mesh; }
    /// Get the framebuffer name
    inline gl::GLuint GetFramebuffer() { return framebuffer; }
    /// Get the texture name
    inline gl::GLuint GetTexture() { return texture; }
    /// Get the texture handle
    inline gl::GLuint64 GetTextureHandle() { return textureHandle; }

 protected:
    gl::GLuint framebuffer;
    gl::GLuint texture;
    gl::GLuint64 textureHandle;
    LightData& lightdata;
    gl::GLint viewportData[4];
};

}  // namespace R3D

#endif  // SRC_RENDER_DATA_SHADOWPASS_H_
