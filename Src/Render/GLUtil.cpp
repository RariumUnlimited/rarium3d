// Copyright RariumUnlimited - Licence : MIT
#include "Render/GLUtil.h"

#include <map>

#include <glm/glm.hpp>

namespace R3D {

void GLUtil::TreatGLError(std::string file, std::string line) {
    gl::GLenum error = gl::glGetError();

    switch (error) {
    case gl::GL_NO_ERROR:
        break;
    case gl::GL_INVALID_ENUM:
        throw std::string("Received gl error : invalid enum - File : " +
                          file + " Line : " + line);
        break;
    case gl::GL_INVALID_VALUE:
        throw std::string("Received gl error : invalid value - File:" +
                          file + " Line:" + line);
        break;
    case gl::GL_INVALID_OPERATION:
        throw std::string("Received gl error : invalid operation - File : " +
                          file + " Line : " + line);
        break;
    case gl::GL_OUT_OF_MEMORY:
        throw std::string("Received gl error : out of memory - File : " +
                          file + " Line : " + line);
        break;
    default:
        throw std::string("Received unknown opengl error");
        break;
    }
}

std::string GLUtil::SourceToStr(gl::GLenum source) {
    static std::map<gl::GLenum, std::string> sourceToStr = {
        { gl::GL_DEBUG_SOURCE_API, "API" },
        { gl::GL_DEBUG_SOURCE_SHADER_COMPILER, "Shader Compiler" },
        { gl::GL_DEBUG_SOURCE_WINDOW_SYSTEM, "Window System" },
        { gl::GL_DEBUG_SOURCE_THIRD_PARTY, "Third Party" },
        { gl::GL_DEBUG_SOURCE_APPLICATION, "Application" },
        { gl::GL_DEBUG_SOURCE_OTHER, "Other" } };

    return sourceToStr[source];
}

std::string GLUtil::TypeToStr(gl::GLenum type) {
    static std::map<gl::GLenum, std::string> typeToStr = {
        { gl::GL_DEBUG_TYPE_ERROR, "Error" },
        { gl::GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR, "Deprecated Behavior" },
        { gl::GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR, "Undefined Behavior" },
        { gl::GL_DEBUG_TYPE_PERFORMANCE, "Performance" },
        { gl::GL_DEBUG_TYPE_PORTABILITY, "Portability" },
        { gl::GL_DEBUG_TYPE_MARKER, "Marker" },
        { gl::GL_DEBUG_TYPE_PUSH_GROUP, "Push Group" },
        { gl::GL_DEBUG_TYPE_POP_GROUP, "Pop Group" },
        { gl::GL_DEBUG_TYPE_OTHER, "Other" } };

    return typeToStr[type];
}

std::string GLUtil::SeverityToStr(gl::GLenum severity) {
    static std::map<gl::GLenum, std::string> severityToStr = {
        { gl::GL_DEBUG_SEVERITY_LOW, "Low" },
        { gl::GL_DEBUG_SEVERITY_MEDIUM, "Medium" },
        { gl::GL_DEBUG_SEVERITY_HIGH, "High" },
        { gl::GL_DEBUG_SEVERITY_NOTIFICATION, "Notification" } };

    return severityToStr[severity];
}

std::string GLUtil::GLSLTypeToStr(gl::GLenum type) {
    static std::map<gl::GLenum, std::string> typeToStr = {
        { gl::GL_FLOAT, "float" },
        { gl::GL_FLOAT_VEC2, "vec2" },
        { gl::GL_FLOAT_VEC3, "vec3" },
        { gl::GL_FLOAT_VEC4, "vec4" },
        { gl::GL_DOUBLE, "double" },
        { gl::GL_DOUBLE_VEC2, "dvec2" },
        { gl::GL_DOUBLE_VEC3, "dvec3" },
        { gl::GL_DOUBLE_VEC4, "dvec4" },
        { gl::GL_INT, "int" },
        { gl::GL_INT_VEC2, "ivec2" },
        { gl::GL_INT_VEC3, "ivec3" },
        { gl::GL_INT_VEC4, "ivec4" },
        { gl::GL_UNSIGNED_INT, "unsigned int" },
        { gl::GL_UNSIGNED_INT_VEC2, "uvec2" },
        { gl::GL_UNSIGNED_INT_VEC3, "uvec3" },
        { gl::GL_UNSIGNED_INT_VEC4, "uvec4" },
        { gl::GL_BOOL, "bool" },
        { gl::GL_BOOL_VEC2, "bvec2" },
        { gl::GL_BOOL_VEC3, "bvec3" },
        { gl::GL_BOOL_VEC4, "bvec4" },
        { gl::GL_FLOAT_MAT2, "mat2" },
        { gl::GL_FLOAT_MAT3, "mat3" },
        { gl::GL_FLOAT_MAT4, "mat4" },
        { gl::GL_FLOAT_MAT2x3, "mat2x3" },
        { gl::GL_FLOAT_MAT2x4, "mat2x4" },
        { gl::GL_FLOAT_MAT3x2, "mat3x2" },
        { gl::GL_FLOAT_MAT3x4, "mat3x4" },
        { gl::GL_FLOAT_MAT4x2, "mat4x2" },
        { gl::GL_FLOAT_MAT4x3, "mat4x3" },
        { gl::GL_DOUBLE_MAT2, "dmat2" },
        { gl::GL_DOUBLE_MAT3, "dmat3" },
        { gl::GL_DOUBLE_MAT4, "dmat4" },
        { gl::GL_DOUBLE_MAT2x3, "dmat2x3" },
        { gl::GL_DOUBLE_MAT2x4, "dmat2x4" },
        { gl::GL_DOUBLE_MAT3x2, "dmat3x2" },
        { gl::GL_DOUBLE_MAT3x4, "dmat3x4" },
        { gl::GL_DOUBLE_MAT4x2, "dmat4x2" },
        { gl::GL_DOUBLE_MAT4x3, "dmat4x3" },
        { gl::GL_SAMPLER_1D, "sampler1D" },
        { gl::GL_SAMPLER_2D, "sampler2D" },
        { gl::GL_SAMPLER_3D, "sampler3D" },
        { gl::GL_SAMPLER_CUBE, "samplerCube" },
        { gl::GL_SAMPLER_1D_SHADOW, "sampler1DShadow" },
        { gl::GL_SAMPLER_2D_SHADOW, "sampler2DShadow" },
        { gl::GL_SAMPLER_1D_ARRAY, "sampler1DArray" },
        { gl::GL_SAMPLER_2D_ARRAY, "sampler2DArray" },
        { gl::GL_SAMPLER_1D_ARRAY_SHADOW, "sampler1DArrayShadow" },
        { gl::GL_SAMPLER_2D_ARRAY_SHADOW, "sampler2DArrayShadow" },
        { gl::GL_SAMPLER_2D_MULTISAMPLE, "sampler2DMS" },
        { gl::GL_SAMPLER_2D_MULTISAMPLE_ARRAY, "sampler2DMSArray" },
        { gl::GL_SAMPLER_CUBE_SHADOW, "samplerCubeShadow" },
        { gl::GL_SAMPLER_BUFFER, "samplerBuffer" },
        { gl::GL_SAMPLER_2D_RECT, "sampler2DRect" },
        { gl::GL_SAMPLER_2D_RECT_SHADOW, "sampler2DRectShadow" },
        { gl::GL_INT_SAMPLER_1D, "isampler1D" },
        { gl::GL_INT_SAMPLER_2D, "isampler2D" },
        { gl::GL_INT_SAMPLER_3D, "isampler3D" },
        { gl::GL_INT_SAMPLER_CUBE, "isamplerCube" },
        { gl::GL_INT_SAMPLER_1D_ARRAY, "isampler1DArray" },
        { gl::GL_INT_SAMPLER_2D_ARRAY, "isampler2DArray" },
        { gl::GL_INT_SAMPLER_2D_MULTISAMPLE, "isampler2DMS" },
        { gl::GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY, "isampler2DMSArray" },
        { gl::GL_INT_SAMPLER_BUFFER, "isamplerBuffer" },
        { gl::GL_INT_SAMPLER_2D_RECT, "isampler2DRect" },
        { gl::GL_UNSIGNED_INT_SAMPLER_1D, "usampler1D" },
        { gl::GL_UNSIGNED_INT_SAMPLER_2D, "usampler2D" },
        { gl::GL_UNSIGNED_INT_SAMPLER_3D, "usampler3D" },
        { gl::GL_UNSIGNED_INT_SAMPLER_CUBE, "usamplerCube" },
        { gl::GL_UNSIGNED_INT_SAMPLER_1D_ARRAY, "usampler2DArray" },
        { gl::GL_UNSIGNED_INT_SAMPLER_2D_ARRAY, "usampler2DArray" },
        { gl::GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE, "usampler2DMS" },
        { gl::GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY, "usampler2DMSArray" },
        { gl::GL_UNSIGNED_INT_SAMPLER_BUFFER, "usamplerBuffer" },
        { gl::GL_UNSIGNED_INT_SAMPLER_2D_RECT, "usampler2DRect" } };

    return typeToStr[type];
}

gl::GLsizei GLUtil::SizeofGLSLType(gl::GLenum type) {
    static std::map<gl::GLenum, gl::GLsizei> typeToSize = {
        { gl::GL_FLOAT, sizeof(float) },
        { gl::GL_FLOAT_VEC2, sizeof(glm::vec2) },
        { gl::GL_FLOAT_VEC3, sizeof(glm::vec3) },
        { gl::GL_FLOAT_VEC4, sizeof(glm::vec4) },
        { gl::GL_DOUBLE, sizeof(double) },
        { gl::GL_DOUBLE_VEC2, sizeof(glm::dvec2) },
        { gl::GL_DOUBLE_VEC3, sizeof(glm::dvec3) },
        { gl::GL_DOUBLE_VEC4, sizeof(glm::dvec4) },
        { gl::GL_INT, sizeof(int) },
        { gl::GL_INT_VEC2, sizeof(glm::ivec2) },
        { gl::GL_INT_VEC3, sizeof(glm::ivec3) },
        { gl::GL_INT_VEC4, sizeof(glm::ivec4) },
        { gl::GL_UNSIGNED_INT, sizeof(unsigned int) },
        { gl::GL_UNSIGNED_INT_VEC2, sizeof(glm::uvec2) },
        { gl::GL_UNSIGNED_INT_VEC3, sizeof(glm::uvec3) },
        { gl::GL_UNSIGNED_INT_VEC4, sizeof(glm::uvec4) },
        { gl::GL_BOOL, sizeof(bool) },
        { gl::GL_BOOL_VEC2, sizeof(glm::bvec2) },
        { gl::GL_BOOL_VEC3, sizeof(glm::bvec3) },
        { gl::GL_BOOL_VEC4, sizeof(glm::bvec4) },
        { gl::GL_FLOAT_MAT2, sizeof(glm::mat2) },
        { gl::GL_FLOAT_MAT3, sizeof(glm::mat3) },
        { gl::GL_FLOAT_MAT4, sizeof(glm::mat4) },
        { gl::GL_FLOAT_MAT2x3, sizeof(glm::mat2x3) },
        { gl::GL_FLOAT_MAT2x4, sizeof(glm::mat2x4) },
        { gl::GL_FLOAT_MAT3x2, sizeof(glm::mat3x2) },
        { gl::GL_FLOAT_MAT3x4, sizeof(glm::mat3x4) },
        { gl::GL_FLOAT_MAT4x2, sizeof(glm::mat4x2) },
        { gl::GL_FLOAT_MAT4x3, sizeof(glm::mat4x3) },
        { gl::GL_DOUBLE_MAT2, sizeof(glm::dmat2) },
        { gl::GL_DOUBLE_MAT3, sizeof(glm::dmat3) },
        { gl::GL_DOUBLE_MAT4, sizeof(glm::dmat4) },
        { gl::GL_DOUBLE_MAT2x3, sizeof(glm::dmat2x3) },
        { gl::GL_DOUBLE_MAT2x4, sizeof(glm::dmat2x4) },
        { gl::GL_DOUBLE_MAT3x2, sizeof(glm::dmat3x2) },
        { gl::GL_DOUBLE_MAT3x4, sizeof(glm::dmat3x4) },
        { gl::GL_DOUBLE_MAT4x2, sizeof(glm::dmat4x2) },
        { gl::GL_DOUBLE_MAT4x3, sizeof(glm::dmat4x3) } };

    return typeToSize[type];
}

gl::GLenum GLUtil::GLSLTypeToBaseType(gl::GLenum type) {
    static std::map<gl::GLenum, gl::GLenum> typeToBase = {
        { gl::GL_FLOAT, gl::GL_FLOAT },
        { gl::GL_FLOAT_VEC2, gl::GL_FLOAT },
        { gl::GL_FLOAT_VEC3, gl::GL_FLOAT },
        { gl::GL_FLOAT_VEC4, gl::GL_FLOAT },
        { gl::GL_DOUBLE, gl::GL_DOUBLE },
        { gl::GL_DOUBLE_VEC2, gl::GL_DOUBLE },
        { gl::GL_DOUBLE_VEC3, gl::GL_DOUBLE },
        { gl::GL_DOUBLE_VEC4, gl::GL_DOUBLE },
        { gl::GL_INT, gl::GL_INT },
        { gl::GL_INT_VEC2, gl::GL_INT },
        { gl::GL_INT_VEC3, gl::GL_INT },
        { gl::GL_INT_VEC4, gl::GL_INT },
        { gl::GL_UNSIGNED_INT, gl::GL_UNSIGNED_INT },
        { gl::GL_UNSIGNED_INT_VEC2, gl::GL_UNSIGNED_INT },
        { gl::GL_UNSIGNED_INT_VEC3, gl::GL_UNSIGNED_INT },
        { gl::GL_UNSIGNED_INT_VEC4, gl::GL_UNSIGNED_INT },
        { gl::GL_BOOL, gl::GL_BOOL },
        { gl::GL_BOOL_VEC2, gl::GL_BOOL },
        { gl::GL_BOOL_VEC3, gl::GL_BOOL },
        { gl::GL_BOOL_VEC4, gl::GL_BOOL },
        { gl::GL_FLOAT_MAT2, gl::GL_FLOAT },
        { gl::GL_FLOAT_MAT3, gl::GL_FLOAT },
        { gl::GL_FLOAT_MAT4, gl::GL_FLOAT },
        { gl::GL_FLOAT_MAT2x3, gl::GL_FLOAT },
        { gl::GL_FLOAT_MAT2x4, gl::GL_FLOAT },
        { gl::GL_FLOAT_MAT3x2, gl::GL_FLOAT },
        { gl::GL_FLOAT_MAT3x4, gl::GL_FLOAT },
        { gl::GL_FLOAT_MAT4x2, gl::GL_FLOAT },
        { gl::GL_FLOAT_MAT4x3, gl::GL_FLOAT },
        { gl::GL_DOUBLE_MAT2, gl::GL_DOUBLE },
        { gl::GL_DOUBLE_MAT3, gl::GL_DOUBLE },
        { gl::GL_DOUBLE_MAT4, gl::GL_DOUBLE },
        { gl::GL_DOUBLE_MAT2x3, gl::GL_DOUBLE },
        { gl::GL_DOUBLE_MAT2x4, gl::GL_DOUBLE },
        { gl::GL_DOUBLE_MAT3x2, gl::GL_DOUBLE },
        { gl::GL_DOUBLE_MAT3x4, gl::GL_DOUBLE },
        { gl::GL_DOUBLE_MAT4x2, gl::GL_DOUBLE },
        { gl::GL_DOUBLE_MAT4x3, gl::GL_DOUBLE } };

    return typeToBase[type];
}

gl::GLuint GLUtil::GLSLTypeToComponentCount(gl::GLenum type) {
    static std::map<gl::GLenum, gl::GLuint> typeToComp = {
        { gl::GL_FLOAT, 1 },
        { gl::GL_FLOAT_VEC2, 2 },
        { gl::GL_FLOAT_VEC3, 3 },
        { gl::GL_FLOAT_VEC4, 4 },
        { gl::GL_DOUBLE, 1 },
        { gl::GL_DOUBLE_VEC2, 2 },
        { gl::GL_DOUBLE_VEC3, 3 },
        { gl::GL_DOUBLE_VEC4, 4 },
        { gl::GL_INT, 1 },
        { gl::GL_INT_VEC2, 2 },
        { gl::GL_INT_VEC3, 3 },
        { gl::GL_INT_VEC4, 4 },
        { gl::GL_UNSIGNED_INT, 1 },
        { gl::GL_UNSIGNED_INT_VEC2, 2 },
        { gl::GL_UNSIGNED_INT_VEC3, 3 },
        { gl::GL_UNSIGNED_INT_VEC4, 4 },
        { gl::GL_BOOL, 1 },
        { gl::GL_BOOL_VEC2, 2 },
        { gl::GL_BOOL_VEC3, 3 },
        { gl::GL_BOOL_VEC4, 4 } };

    return typeToComp[type];
}

}  // namespace R3D
