// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RENDER_GLUTIL_H_

#define SRC_RENDER_GLUTIL_H_

#include <glbinding/Binding.h>
#include <glbinding/gl/gl.h>

#include <string>

#include <Rarium/Tools/Util.h>

namespace R3D {

#define CheckGLError() GLUtil::TreatGLError(RR::ToString(__FILE__), RR::ToString(__LINE__))

class GLUtil {
 public:
    /**
     * Check if there is an opengl error, throw an exception if it is the case.
     */
    static void TreatGLError(std::string file, std::string line);
    /**
     *    Get the string corresponding to an opengl debug source
     *    @param source OpenGL debug source
     *    @return String containing the name of this debug source
     */
    static std::string SourceToStr(gl::GLenum source);
    /**
     *    Get the string corresponding to an opengl debug type
     *    @param type OpenGL debug type
     *    @return String containing the name of this debug type
     */
    static std::string TypeToStr(gl::GLenum type);
    /**
     *    Get the string corresponding to an opengl debug severity
     *    @param severity OpenGL debug severity
     *    @return String containing the name of this debug severity
     */
    static std::string SeverityToStr(gl::GLenum severity);
    /**
     *    Get the string corresponding to a glsl type
     *    @param type GLSL type
     *    @return String containing the name of this type
     */
    static std::string GLSLTypeToStr(gl::GLenum type);
    /**
     *    Get the size of a glsl type on byte
     *    @param GLSL type
     *    @return The size in byte of the type
     */
    static gl::GLsizei SizeofGLSLType(gl::GLenum type);
    /**
     *    Get the base type of a glsltype
     *    @param type GLSL type
     *    @return The base type of type
     */
    static gl::GLenum GLSLTypeToBaseType(gl::GLenum type);
    /**
     *    Get the number of component of a type
     *    @param type GLSL Type
     *    @return Number of component of type
     */
    static gl::GLuint GLSLTypeToComponentCount(gl::GLenum type);
};

}  // namespace R3D

#endif  // SRC_RENDER_GLUTIL_H_
