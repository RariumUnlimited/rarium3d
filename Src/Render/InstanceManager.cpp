// Copyright RariumUnlimited - Licence : MIT
#include "Render/InstanceManager.h"

#include <unordered_set>

#include "Assets/Collection.h"

namespace R3D {

InstanceManager::InstanceManager() { }

void InstanceManager::SetupVAO() {
    std::unordered_set<std::string> techniques;
    for (Instance* i : instances)
        techniques.insert(i->Technique);

    for (Pass* p : passes)
        techniques.insert(p->GetTechnique());

    for (std::string s : techniques) {
        gl::GLuint vao;
        gl::glCreateVertexArrays(1, &vao);
        RR::Catalog<Technique>& techcata = Collection::Techniques();
        Technique* tech = Collection::Techniques()[s];
        vaos[tech->GetProgram()] = vao;
    }

    for (std::pair<Program*, gl::GLuint> vao : vaos)
        SetupVertexAttrib(vao);
}

void InstanceManager::Purge() {
    instances.clear();
    vaos.clear();
    passes.clear();
    commands.clear();
}

void InstanceManager::Passes() {
    for (Pass* p : passes) {
        Technique* tech = Collection::Techniques()[p->GetTechnique()];
        gl::glUseProgram(tech->GetProgram()->GetProgram());
        gl::glBindVertexArray(vaos[tech->GetProgram()]);

        p->Setup();

        for (CommandData cmdData : commands) {
            if (cmdData.Count > 0) {
                gl::glBindBuffer(gl::GL_DRAW_INDIRECT_BUFFER, cmdData.Buffer);

                DrawCall(cmdData.Count);
            }
        }

        p->CleanUp();
    }

    gl::glBindVertexArray(0);
}

void InstanceManager::Draw() {
    for (CommandData cmdData : commands) {
        if (cmdData.Count > 0) {
            Technique* tech = Collection::Techniques()[cmdData.Technique];
            gl::glUseProgram(tech->GetProgram()->GetProgram());
            gl::glBindVertexArray(vaos[tech->GetProgram()]);
            gl::glBindBuffer(gl::GL_DRAW_INDIRECT_BUFFER, cmdData.Buffer);

            DrawCall(cmdData.Count);
        }
    }

    gl::glBindVertexArray(0);
}

}  // namespace R3D
