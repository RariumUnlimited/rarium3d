// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RENDER_INSTANCEMANAGER_H_

#define SRC_RENDER_INSTANCEMANAGER_H_

#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include "Assets/Instance.h"
#include "Assets/Program.h"
#include "Render/Data/Pass.h"

namespace R3D {

/// Structure that represent a draw command to be executed.
struct CommandData {
    std::string Technique;  ///< Technique of the command
    gl::GLuint Buffer;  ///< Indirect buffer name
    gl::GLuint Count;  ///< How many instance to draw
};

/// Manage instancse to draw, create buffers, updates them, ...
class InstanceManager {
 public:
    /// Build a new instance manager (and create the VAO)
    InstanceManager();
    /**
     * Add a new instance to the manager
     * @param instance Instance to add
     */
    virtual void AddInstance(Instance* instance) {
        instances.push_back(instance);
    }
    /**
     * Delete an instance from the manager
     * @param instance Instance to delete
     */
    virtual void DeleteInstance(Instance* instance) {
        throw std::string("Deleting instance not supported");
    }
    /**
     * Add a new pass to the manager
     * @param pass Pass to add
     */
    void AddPass(Pass* pass) {
        passes.push_back(pass);
    }

    /**
     * Setup VAOs (one for each technique needed to draw instance of this manager)
     */
    void SetupVAO();

    /**
     * Build data neccessary for rendering (main data as well as per instance data)
     */
    virtual void Build() = 0;

    /**
     * Update the data of the manager
     */
    virtual void Update() = 0;

    /**
     * Purge all the datas and instances of the manager
     */
    virtual void Purge();

    /**
     * Draw all instances for each passes
     */
    void Passes();

    /**
     * Draw all instances with their respectives technique
     */
    void Draw();

    /**
     *  Call the opengl multi draw indirect command for this instance manager
     *  @param Count
     */
    virtual void DrawCall(int count) = 0;

 protected:
    /**
     * Setup the vertex attrib for a vao
     * @param pair A pair : program (technique) -> is VAO
     */
    virtual void SetupVertexAttrib(std::pair<Program*, gl::GLuint> pair) = 0;

    std::vector<Instance*> instances;  ///< All instances handled by this manager
    std::unordered_map<Program*, gl::GLuint> vaos;  ///< For each technique a VAO
    std::vector<CommandData> commands;  ///< List of command data of this manager
    std::vector<Pass*> passes;  ///< Additional pass for this manager
};

}  // namespace R3D

#endif  // SRC_RENDER_INSTANCEMANAGER_H_
