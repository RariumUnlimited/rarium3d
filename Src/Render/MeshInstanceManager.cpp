// Copyright RariumUnlimited - Licence : MIT
#include "Render/MeshInstanceManager.h"

#include <omp.h>

#include <atomic>
#include <functional>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include "Assets/Collection.h"

namespace R3D {


void MeshInstanceManager::Purge() {
    if (instances.size() > 0) {
        InstanceManager::Purge();
        cmdBuffers.clear();
        vertexData.reset(nullptr);
        indexData.reset(nullptr);
        instData.reset(nullptr);
        sortedInstances.clear();
        instanceBuilt = 0;
    }
}

void MeshInstanceManager::Update() {
    if (instances.size() > 0 && instances.size() == instanceBuilt) {
        InstanceBufferData* data =
                reinterpret_cast<InstanceBufferData*>(instData->GetMap());

        int i = 0;
        for (Instance* inst : sortedInstances) {
            MeshData& d = std::get<MeshData>(inst->Data);
            data[i].ModelMatrix = d.ModelMatrix;
            data[i].MatAmbient = d.Mesh->GetMaterial().Ka;
            data[i].MatSpecular = d.Mesh->GetMaterial().Ks;
            data[i].MatGloss = d.Mesh->GetMaterial().Ns;
            data[i].MatTexture = d.Mesh->GetMaterial().KdMap->GetHandle();
            ++i;
        }
    } else if (instances.size() > 0 && instances.size() != instanceBuilt) {
        throw std::string("Trying to update manage with " +
                          std::to_string(instances.size()) +
                          " instances when only "+
                          std::to_string(instanceBuilt) +
                          " have been built.");
    }
}

void MeshInstanceManager::Build() {
    if (instances.size() == 0)
        return;

    // To optimise the draw commands, we need to sort instances by technique and
    // by their mesh name, so that every instance with the same technique and mesh
    // are draw using only one command
    sortedInstances.clear();
    sortedInstances.insert(instances.cbegin(), instances.cend());


    std::vector<Mesh*> meshToBuild;
    std::unordered_map<Mesh*, VertexArrayData> meshArrayData;

    int totalVertices = 0;  ///< How many "vertices" we currently have in meshToBuild
    int totalIndices = 0;  ///< How many "indices" we currently have in meshToBuild

    // Making sure we build only unique mesh and only the one that are used
    // At the same time we get their respective data about how they will be in the vertex array
    for (unsigned int i = 0; i < instances.size(); ++i) {
        MeshData& d = std::get<MeshData>(instances[i]->Data);

        if (meshArrayData.find(d.Mesh) == meshArrayData.cend()) {
            VertexArrayData vad;
            vad.Mesh = d.Mesh;
            vad.BaseVertex = totalVertices;
            vad.BaseIndex = totalIndices;

            totalVertices += d.Mesh->GetVertices().size();
            totalIndices += d.Mesh->GetIndices().size();

            meshArrayData.insert(std::make_pair(d.Mesh, vad));
            meshToBuild.push_back(d.Mesh);
        }
    }

    // Buffers initiliaztion
    vertexData = std::make_unique<Buffer>(static_cast<gl::GLsizei>(totalVertices * sizeof(Mesh::Vertex)));
    indexData = std::make_unique<Buffer>(static_cast<gl::GLsizei>(totalIndices * sizeof(unsigned int)));
    instData = std::make_unique<Buffer>(static_cast<gl::GLsizei>(sortedInstances.size() * sizeof(InstanceBufferData)));

    // Uploading instance data to the GPU
    instanceBuilt = sortedInstances.size();

    Update();

    // Uploading all mesh data to the GPU
    Mesh::Vertex* vertexDataMap =
            reinterpret_cast<Mesh::Vertex*>(vertexData->GetMap());
    unsigned int* indexDataMap =
            reinterpret_cast<unsigned int*>(indexData->GetMap());

    totalVertices = 0;
    totalIndices = 0;

    for (unsigned int i = 0; i < meshToBuild.size(); ++i) {
        Mesh* mesh = meshToBuild[i];

        #pragma omp parallel
        {
            #pragma omp for
            for (int j = 0; j < mesh->GetVertices().size(); ++j) {
                vertexDataMap[totalVertices + j] = mesh->GetVertices()[j];
            }

            #pragma omp for
            for (int j = 0; j < mesh->GetIndices().size(); ++j) {
                indexDataMap[totalIndices + j] = mesh->GetIndices()[j];
            }
        }

        totalVertices += mesh->GetVertices().size();
        totalIndices += mesh->GetIndices().size();
    }

    // Building the command buffer
    std::string currentTechnique = (*sortedInstances.begin())->Technique;
    Mesh* currentMesh =
            std::get<MeshData>((*(sortedInstances.begin()))->Data).Mesh;
    gl::GLuint instanceCount = 0;
    gl::GLuint baseInstance = 0;

    std::vector<gl::GLuint> cmdData;

    auto addCmd = [&]() {
        cmdData.push_back(currentMesh->GetIndices().size());
        cmdData.push_back(instanceCount);
        cmdData.push_back(meshArrayData[currentMesh].BaseIndex);
        cmdData.push_back(meshArrayData[currentMesh].BaseVertex);
        cmdData.push_back(baseInstance);
        baseInstance += instanceCount;
        instanceCount = 0;
    };

    auto addTechnique = [&]() {
        cmdBuffers.emplace_back(
                    std::make_unique<Buffer>(cmdData.size() * sizeof(gl::GLuint)));
        cmdBuffers.back()->Update(cmdData.data(),
                                  cmdData.size() * sizeof(gl::GLuint));
        CommandData dataCommand;
        dataCommand.Technique = currentTechnique;
        dataCommand.Buffer = cmdBuffers.back()->GetName();
        dataCommand.Count = cmdData.size() / 5;
        commands.push_back(dataCommand);
        cmdData.clear();
    };

    for (Instance* i : sortedInstances) {
        MeshData& d = std::get<MeshData>(i->Data);

        if (d.Mesh->GetName() != currentMesh->GetName()) {
            addCmd();
            currentMesh = d.Mesh;
        }

        if (i->Technique != currentTechnique) {
            addTechnique();
            currentTechnique = i->Technique;
        }

        ++instanceCount;
    }

    addCmd();
    addTechnique();
}

void MeshInstanceManager::SetupVertexAttrib(std::pair<Program*, gl::GLuint> pair) {
    if (instances.size() > 0) {
        Program* program = pair.first;
        gl::glBindVertexArray(pair.second);
        gl::glBindBuffer(gl::GL_ARRAY_BUFFER, vertexData->GetName());

        if (program->GetInputs().find("Position") != program->GetInputs().end()) {
            gl::glVertexAttribPointer(program->GetInputs()["Position"].Location, 3,
                    gl::GL_FLOAT, gl::GL_FALSE, sizeof(Mesh::Vertex),
                    reinterpret_cast<void *>(offsetof(Mesh::Vertex, Mesh::Vertex::Position)));
            gl::glEnableVertexAttribArray(program->GetInputs()["Position"].Location);
        }

        if (program->GetInputs().find("TexCoord") != program->GetInputs().end()) {
            gl::glVertexAttribPointer(program->GetInputs()["TexCoord"].Location, 2,
                    gl::GL_FLOAT, gl::GL_FALSE, sizeof(Mesh::Vertex),
                    reinterpret_cast<void *>(offsetof(Mesh::Vertex, Mesh::Vertex::TexCoord)));
            gl::glEnableVertexAttribArray(program->GetInputs()["TexCoord"].Location);
        }

        if (program->GetInputs().find("Normal") != program->GetInputs().end()) {
            gl::glVertexAttribPointer(program->GetInputs()["Normal"].Location, 3,
                    gl::GL_FLOAT, gl::GL_FALSE, sizeof(Mesh::Vertex),
                    reinterpret_cast<void *>(offsetof(Mesh::Vertex, Mesh::Vertex::Normal)));
            gl::glEnableVertexAttribArray(program->GetInputs()["Normal"].Location);
        }

        gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, indexData->GetName());


        gl::glBindBuffer(gl::GL_ARRAY_BUFFER, instData->GetName());

        if (program->GetInputs().find("ModelMatrix") != program->GetInputs().end()) {
            for (unsigned int i = 0; i < 4; ++i) {
                gl::glVertexAttribPointer(program->GetInputs()["ModelMatrix"].Location + i, 4, gl::GL_FLOAT,
                        gl::GL_FALSE, sizeof(InstanceBufferData),
                        reinterpret_cast<void *>(offsetof(InstanceBufferData, InstanceBufferData::ModelMatrix) + (i * 4) * sizeof(float)));
                gl::glEnableVertexAttribArray(program->GetInputs()["ModelMatrix"].Location + i);
                gl::glVertexAttribDivisor(program->GetInputs()["ModelMatrix"].Location + i, 1);
            }
        }

        if (program->GetInputs().find("MatAmbient") != program->GetInputs().end()) {
            gl::glVertexAttribPointer(program->GetInputs()["MatAmbient"].Location, 3,
                    gl::GL_FLOAT, gl::GL_FALSE, sizeof(InstanceBufferData),
                    reinterpret_cast<void *>(offsetof(InstanceBufferData, InstanceBufferData::MatAmbient)));
            gl::glEnableVertexAttribArray(program->GetInputs()["MatAmbient"].Location);
            gl::glVertexAttribDivisor(program->GetInputs()["MatAmbient"].Location, 1);
        }

        if (program->GetInputs().find("MatSpecular") != program->GetInputs().end()) {
            gl::glVertexAttribPointer(program->GetInputs()["MatSpecular"].Location, 3,
                    gl::GL_FLOAT, gl::GL_FALSE, sizeof(InstanceBufferData),
                    reinterpret_cast<void *>(offsetof(InstanceBufferData, InstanceBufferData::MatSpecular)));
            gl::glEnableVertexAttribArray(program->GetInputs()["MatSpecular"].Location);
            gl::glVertexAttribDivisor(program->GetInputs()["MatSpecular"].Location, 1);
        }

        if (program->GetInputs().find("MatGloss") != program->GetInputs().end()) {
            gl::glVertexAttribPointer(program->GetInputs()["MatGloss"].Location, 1,
                    gl::GL_UNSIGNED_INT, gl::GL_FALSE, sizeof(InstanceBufferData),
                    reinterpret_cast<void *>(offsetof(InstanceBufferData, InstanceBufferData::MatGloss)));
            gl::glEnableVertexAttribArray(program->GetInputs()["MatGloss"].Location);
            gl::glVertexAttribDivisor(program->GetInputs()["MatGloss"].Location, 1);
        }

        if (program->GetInputs().find("MatTexture") != program->GetInputs().end()) {
            gl::glVertexAttribLPointer(program->GetInputs()["MatTexture"].Location, 1,
                    gl::GL_UNSIGNED_INT64_ARB, sizeof(InstanceBufferData),
                    reinterpret_cast<void *>(offsetof(InstanceBufferData, InstanceBufferData::MatTexture)));
            gl::glEnableVertexAttribArray(program->GetInputs()["MatTexture"].Location);
            gl::glVertexAttribDivisor(program->GetInputs()["MatTexture"].Location, 1);
        }

        gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
        gl::glBindVertexArray(0);
    }
}

void MeshInstanceManager::DrawCall(int count) {
    gl::glMultiDrawElementsIndirect(gl::GL_TRIANGLES,
                                    gl::GL_UNSIGNED_INT,
                                    0,
                                    count,
                                    0);
}

}  // namespace R3D
