// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RENDER_MESHINSTANCEMANAGER_H_

#define SRC_RENDER_MESHINSTANCEMANAGER_H_

#include <memory>
#include <set>
#include <utility>
#include <vector>

#include "Render/InstanceManager.h"
#include "Render/Data/Buffer.h"

namespace R3D {

class MeshInstanceManager : public InstanceManager {
 public:
    void Build() override;
    void Purge() override;
    void Update() override;
    void DrawCall(int count) override;

 protected:
    void SetupVertexAttrib(std::pair<Program*, gl::GLuint> pair) override;


    // To optimise the draw commands, we need to sort instances by technique and
    // by their mesh name, so that every instance with the same technique and mesh
    // are draw using only one command. This is why we use a set and a custom comparison
    struct MeshInstanceCmp {
        bool operator()(Instance* a, Instance* b) const {
            MeshData& adata = std::get<MeshData>(a->Data);
            MeshData& bdata = std::get<MeshData>(b->Data);
            if (a->Technique != b->Technique)
                return a->Technique < b->Technique;
            else if (adata.Mesh->GetName() != bdata.Mesh->GetName())
                return adata.Mesh->GetName() < bdata.Mesh->GetName();
            else
                return a < b;
        }
    };

    std::set<Instance*, MeshInstanceCmp> sortedInstances;  ///< Instances sorted by technique and mesh
    std::unique_ptr<Buffer> vertexData;  ///< Buffer that contains all vertices of all meshes
    std::unique_ptr<Buffer> indexData;  ///< Buffer that constains all indices of all meshes
    std::unique_ptr<Buffer> instData;  ///< Buffer that contains per instance data of all instances
    std::vector<std::unique_ptr<Buffer>> cmdBuffers;  ///< Buffers containing command for indirect rendering, cmdBuffers[i] <==> commands[i], one buffer per technique
    int instanceBuilt;  ///< How many instance there was when we called Build

    /// Represent the data in the instData buffer
    struct InstanceBufferData {
        glm::mat4 ModelMatrix;  ///< Model matrix a of mesh
        glm::vec3 MatAmbient;  ///< Ambient component of the material
        glm::vec3 MatSpecular;  ///< Specular component of the material
        unsigned int MatGloss;  ///< Shininess of the material
        gl::GLuint64EXT MatTexture;  ///< Diffuse texture of the material
    };

    /// Data about how a mesh is stored in the vertex/index array
    struct VertexArrayData {
        R3D::Mesh* Mesh;
        int BaseVertex;
        int BaseIndex;
    };
};

}  // namespace R3D

#endif  // SRC_RENDER_MESHINSTANCEMANAGER_H_
