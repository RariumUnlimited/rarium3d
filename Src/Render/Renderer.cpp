// Copyright RariumUnlimited - Licence : MIT
#include "Render/Renderer.h"

#include <glbinding/ContextInfo.h>
#include <glbinding/Meta.h>

#include <sstream>
#include <set>
#include <string>
#include <utility>
#include <vector>

#include <Rarium/Tools/FileLoader.h>
#include <Rarium/Tools/Util.h>
#include <Rarium/Ui/UiElements/TextElement.h>
#include <Rarium/Ui/UiElements/ButtonElement.h>
#include <Rarium/Ui/UiElements/MultiTextElement.h>

#include "Assets/Collection.h"
#include "Assets/FileLoader.h"
#include "Render/TextureInstanceManager.h"
#include "Render/MeshInstanceManager.h"

namespace R3D {

bool Renderer::Init(unsigned int width, unsigned int height) {
    this->width = width;
    this->height = height;

    glbinding::Binding::initialize();

    // Get context info
    RR::Logger::Log() << "GL Info :\n";
    RR::Logger::Log() << gl::glGetString(gl::GL_VENDOR) << "\n";
    RR::Logger::Log() << gl::glGetString(gl::GL_RENDERER) << "\n";
    RR::Logger::Log() << gl::glGetString(gl::GL_VERSION) << "\n";
    RR::Logger::Log() << gl::glGetString(gl::GL_SHADING_LANGUAGE_VERSION) << "\n";

    // Get the list of extensions available
    std::set<std::string> extensions;
    int numExt;
    glGetIntegerv(gl::GL_NUM_EXTENSIONS, &numExt);
    for (int i = 0; i < numExt; ++i) {
        extensions.insert(RR::ToString(gl::glGetStringi(gl::GL_EXTENSIONS, i)));
    }

    // Check that all extensions needed are present
    if (extensions.find("GL_NV_gpu_shader5") == extensions.end())
        throw std::string("GL_NV_gpu_shader5 is not available !");

    if (extensions.find("GL_ARB_bindless_texture") == extensions.end())
        throw std::string("GL_ARB_bindless_texture is not available !");

    debugProc = gl::GLDEBUGPROC(&DebugProc);
    gl::glDebugMessageCallback(debugProc, nullptr);
    gl::glDisable(gl::GL_CULL_FACE);
    gl::glCullFace(gl::GL_BACK);
    gl::glFrontFace(gl::GL_CCW);
    gl::glEnable(gl::GL_DEPTH_TEST);
    gl::glDepthFunc(gl::GL_LEQUAL);
    gl::glClearColor(0, 0, 0, 1);
    gl::glClearDepthf(1.0);
    gl::glEnable(gl::GL_BLEND);
    gl::glBlendFunc(gl::GL_SRC_ALPHA, gl::GL_ONE_MINUS_SRC_ALPHA);

    gl::glEnable(gl::GL_TEXTURE_CUBE_MAP_SEAMLESS);

    globalDataBuffer = std::make_unique<Buffer>(sizeof(GlobalData), "Global Data Buffer");
    globalData.ScreenResolution = glm::vec2(width, height);
    globalDataBuffer->Update(&globalData, sizeof(GlobalData));
    gl::glBindBufferBase(gl::GL_UNIFORM_BUFFER,
                     BlockBinding::GlobalData,
                     globalDataBuffer->GetName());


    lightDataBuffer = std::make_unique<Buffer>(sizeof(LightData), "Light Data Buffer");
    gl::glBindBufferBase(gl::GL_UNIFORM_BUFFER,
                     BlockBinding::LightData,
                     lightDataBuffer->GetName());

    staticManagers.insert(std::make_pair(Instance::T::Text,
                                         std::make_unique<TextureInstanceManager>()));
    staticManagers.insert(std::make_pair(Instance::T::Mesh,
                                         std::make_unique<MeshInstanceManager>()));
    dynamicManagers.insert(std::make_pair(Instance::T::Text,
                                          std::make_unique<TextureInstanceManager>()));
    dynamicManagers.insert(std::make_pair(Instance::T::Mesh,
                                          std::make_unique<MeshInstanceManager>()));

    CheckGLError();

    LoadTechniques();

    return true;
}

void Renderer::DrawFrame(float delta) {
    gl::glMemoryBarrier(gl::GL_CLIENT_MAPPED_BUFFER_BARRIER_BIT);

    gl::glViewport(0, 0, width, height);

    gl::glClear(gl::GL_COLOR_BUFFER_BIT | gl::GL_DEPTH_BUFFER_BIT);

    globalData.ViewMatrix = scene->GetCamera()->GetViewMatrix();
    globalData.ProjectionMatrix = scene->GetCamera()->GetProjectionMatrix();
    globalDataBuffer->Update(&globalData, sizeof(GlobalData));

    for (const auto& p : dynamicManagers) {
        p.second->Update();
    }

    CheckGLError();

    for (const auto& p : staticManagers) {
        p.second->Passes();
    }

    for (const auto& p : dynamicManagers) {
        p.second->Passes();
    }

    CheckGLError();

    for (const auto& p : staticManagers) {
        p.second->Draw();
    }

    for (const auto& p : dynamicManagers) {
        p.second->Draw();
    }


    CheckGLError();
}

void Renderer::Unload() {
    UnloadAssets();
}

void Renderer::UnloadAssets() {
    for (const auto& p : staticManagers)
        p.second->Purge();
    for (const auto& p : dynamicManagers)
        p.second->Purge();
}

void Renderer::SetupScene(Scene* scene) {
    UnloadAssets();
    std::set<std::string> techs;
    this->scene = scene;
    this->lightData = scene->GetLightData();
    lightDataBuffer->Update(&lightData, sizeof(LightData));

    std::vector<Instance*> instances = scene->GetInstances();
    for (Instance* inst : instances) {
        if (inst->Technique != "") {
            InstanceManager* mana = inst->Dynamic ?
                                    dynamicManagers[inst->Type].get() :
                                    staticManagers[inst->Type].get();
            mana->AddInstance(inst);
            techs.insert(inst->Technique);
        }
    }

    for (Pass* p : scene->GetPasses()) {
        staticManagers[p->GetType()]->AddPass(p);
        dynamicManagers[p->GetType()]->AddPass(p);
    }

    CheckGLError();

    for (const auto& p : staticManagers) {
        p.second->Build();
        p.second->SetupVAO();
    }
    for (const auto& p : dynamicManagers) {
        p.second->Build();
        p.second->SetupVAO();
    }

    CheckGLError();
}

void Renderer::LoadTechniques() {
    std::istringstream iss(RR::LoadFileAsString("Techniques.dat"));

    std::string line;

    while (std::getline(iss, line)) {
        if (line == "")
            continue;

        std::vector<std::string> tech = RR::Split(line, ' ', '\r');

        if (tech.size() != 3)
            RR::Logger::Log() << "Program : " << line <<
                " is invalid, only VS and FS supported at this time.\n";

        Shader* vs = Collection::Shaders().Add(tech[1], tech[1]);

        Shader* fs = Collection::Shaders().Add(tech[2], tech[2]);

        Collection::Programs().Add(tech[0], vs, fs)->ExtractData();
    }

    CheckGLError();
}

void GL_APIENTRY Renderer::DebugProc(gl::GLenum source, gl::GLenum type, gl::GLuint id,
                                     gl::GLenum severity, gl::GLsizei length,
                                     const gl::GLchar* message, void* userParam) {
    RR::Logger::Log() << "OpenGL message, Source " <<
                         GLUtil::SourceToStr(source) <<
                         ", Type " << GLUtil::TypeToStr(type) <<
                         ", Severity " << GLUtil::SeverityToStr(severity) <<
                         " : " << RR::ToString(message) << "\n";
}

}  // namespace R3D
