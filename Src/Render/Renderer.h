// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RENDER_RENDERER_H_

#define SRC_RENDER_RENDERER_H_

#include <glbinding/gl/gl.h>
#include <glbinding/Binding.h>

#include <memory>
#include <unordered_map>

#include <Rarium/Tools/Catalog.h>
#include <Rarium/Tools/Log.h>
#include <Rarium/Ui/Ui.h>
#include <Rarium/Ui/UiElements/TextElement.h>

#include "Assets/Program.h"
#include "Assets/Scene/Scene.h"
#include "Assets/Texture.h"
#include "Render/Data/Buffer.h"
#include "Render/Data/LightData.h"
#include "Render/GLUtil.h"
#include "Window/WindowFunction.h"
#include "Render/InstanceManager.h"

namespace R3D {

/**
 *    Structure used to store data that may be of use to all shaders
 */
struct GlobalData {
    glm::mat4 ViewMatrix;  ///< View matrix
    glm::mat4 ProjectionMatrix;  ///< Projection matrix
    glm::vec2 ScreenResolution;  ///< Resolution of the viewport
};



/// Binding of blocks that can be used by any shader
namespace BlockBinding {
    enum BlockBinding {
        GlobalData = 0,
        LightData = 1
    };
}

class Renderer {
 public:
    /**
    *    Initialize the renderer
    *    @param width Width of the window
    *    @param height Height of the window
    *    @return True if the renderer successfully init
    */
    bool Init(unsigned int width, unsigned int height);
    /**
    *    Draw a frame
    *    @param delta Delta time between the last frame and now
    */
    void DrawFrame(float delta);
    /// Unload all resources
    void Unload();


    /**
    *    Setup a scene to display by the renderer
    *    @param scene Pointer to the Scene
    */
    void SetupScene(Scene* scene);

    /**
    *    Opengl Debug function
    */
    static void GL_APIENTRY DebugProc(gl::GLenum source, gl::GLenum type, gl::GLuint id,
                                      gl::GLenum severity, gl::GLsizei length,
                                      const gl::GLchar* message, void* userParam);

    gl::GLDEBUGPROC debugProc;  ///< Function used when debugging OpenGL

 protected:
    unsigned int width;  ///< Screen width
    unsigned int height;  ///< Screen height
    Scene* scene;  ///< Current scene viewed

    std::unique_ptr<Buffer> globalDataBuffer;  ///< Pointer to a buffer that contains the data of globalData
    GlobalData globalData;  ///< Global data of shaders

    std::unique_ptr<Buffer> lightDataBuffer;  ///< Pointer to a buffer that contains the data of lightData
    LightData lightData;  ///< Global data of shaders

    std::unordered_map<Instance::T, std::unique_ptr<InstanceManager>> staticManagers;  ///< InstanceManagers for all type of static instance we have
    std::unordered_map<Instance::T, std::unique_ptr<InstanceManager>> dynamicManagers;  ///< InstanceManagers for all type of dynamic instance we have

    /// Load techniques
    void LoadTechniques();
    /// Unload assets for current scene
    void UnloadAssets();
};

}  // namespace R3D

#endif  // SRC_RENDER_RENDERER_H_
