// Copyright RariumUnlimited - Licence : MIT
#include "Render/TextureInstanceManager.h"

#include <set>
#include <string>
#include <vector>

#include "Assets/Collection.h"

namespace R3D {

void TextureInstanceManager::Purge() {
    if (instances.size() > 0) {
        InstanceManager::Purge();
        cmdBuffers.clear();
        data.reset(nullptr);
        instanceBuilt = 0;
    }
}

void TextureInstanceManager::Update() {
    if (instances.size() > 0 && instances.size() == instanceBuilt) {
        TextureBufferData* data = reinterpret_cast<TextureBufferData*>(this->data->GetMap());
        for (unsigned int i = 0; i < instances.size(); ++i) {
            data[i].Texture = std::get<TexData>(instances[i]->Data).Tex->GetHandle();
            data[i].Position = std::get<TexData>(instances[i]->Data).Position;
            data[i].Size = std::get<TexData>(instances[i]->Data).Size;
            data[i].Z = glm::vec2(std::get<TexData>(instances[i]->Data).Z);
        }
    } else if (instances.size() > 0 && instances.size() != instanceBuilt) {
        throw std::string("Trying to update manage with " +
                          std::to_string(instances.size()) +
                          "When only "+
                          std::to_string(instanceBuilt) +
                          " have been built.");
    }
}

void TextureInstanceManager::Build() {
    if (instances.size() > 0) {
        instanceBuilt = instances.size();
        std::set<std::string> techs;
        data = std::make_unique<Buffer>(instances.size() * sizeof(TextureBufferData));
        TextureBufferData* data = reinterpret_cast<TextureBufferData*>(this->data->GetMap());

        for (unsigned int i = 0; i < instances.size(); ++i) {
            data[i].Texture = std::get<TexData>(instances[i]->Data).Tex->GetHandle();
            data[i].Position = std::get<TexData>(instances[i]->Data).Position;
            data[i].Size = std::get<TexData>(instances[i]->Data).Size;
            data[i].Z = glm::vec2(std::get<TexData>(instances[i]->Data).Z);
            techs.insert(instances[i]->Technique);
        }

        /*
         * Instances are not sorted by technique, example (instance id -> technique name):
         * a -> 1
         * b -> 1
         * c -> 2
         * d -> 1
         * e -> 2
         * f -> 2
         * All instances that uses the same technique are drawn at the same time (to avoid so many UseProgram).
         * But each instance needs to get its data from the instance data buffer at the right place, so we use a command for each "block" of instances using the same technique.
         * For technique 1 of our example :
         * 2 Command
         * 1st : 4, 2, 0, 0
         * 2nd : 4, 1, 0, 3
         * For technique 2 of our example :
         * 2 Command
         * 1st : 4, 1, 0, 2
         * 2nd : 4, 2, 0, 4
         *
         * Note : Struct of the command (in our case for multi draw arrays indirect) :
         * Vertex Count (In our case texture always have 4 vertex), Instance count, Base Vertex, Base Instance
         */

        for (std::string s : techs) {
            gl::GLuint cpt = 0;
            gl::GLuint baseI = 0;
            std::vector<gl::GLuint> cmdData;
            std::string current = "";

            for (unsigned int i = 0; i < instances.size(); ++i) {
                Instance* inst = instances[i];

                if (inst->Technique != current) {
                    if (current != "" && current == s) {
                        cmdData.push_back(4);
                        cmdData.push_back(cpt);
                        cmdData.push_back(0);
                        cmdData.push_back(baseI);
                    }

                    if (inst->Technique == s) {
                        cpt = 0;
                        baseI = i;
                    }

                    current = inst->Technique;
                }

                if (current == s) {
                    ++cpt;
                }
            }

            if (current == s) {
                cmdData.push_back(4);
                cmdData.push_back(cpt);
                cmdData.push_back(0);
                cmdData.push_back(baseI);
            }

            cmdBuffers.emplace_back(std::make_unique<Buffer>(cmdData.size() * sizeof(gl::GLuint)));
            cmdBuffers.back()->Update(cmdData.data(), cmdData.size() * sizeof(gl::GLuint));
            CommandData dataCommand;
            dataCommand.Technique = s;
            dataCommand.Buffer = cmdBuffers.back()->GetName();
            dataCommand.Count = cmdData.size() / 4;
            commands.push_back(dataCommand);
        }
    }
}

void TextureInstanceManager::SetupVertexAttrib(std::pair<Program*, gl::GLuint> pair) {
    if (instances.size() > 0) {
        Program* program = pair.first;
        gl::glBindVertexArray(pair.second);
        gl::glBindBuffer(gl::GL_ARRAY_BUFFER, data->GetName());

        if (program->GetInputs().find("Texture") != program->GetInputs().end()) {
            gl::glVertexAttribLPointer(program->GetInputs()["Texture"].Location, 1,
                    gl::GL_UNSIGNED_INT64_ARB, sizeof(TextureBufferData),
                    reinterpret_cast<void *>(offsetof(TextureBufferData, TextureBufferData::Texture)));
            gl::glEnableVertexAttribArray(program->GetInputs()["Texture"].Location);
            gl::glVertexAttribDivisor(program->GetInputs()["Texture"].Location, 1);
        }

        if (program->GetInputs().find("Position") != program->GetInputs().end()) {
            gl::glVertexAttribPointer(program->GetInputs()["Position"].Location, 2,
                    gl::GL_FLOAT, gl::GL_FALSE, sizeof(TextureBufferData),
                    reinterpret_cast<void *>(offsetof(TextureBufferData, TextureBufferData::Position)));
            gl::glEnableVertexAttribArray(program->GetInputs()["Position"].Location);
            gl::glVertexAttribDivisor(program->GetInputs()["Position"].Location, 1);
        }

        if (program->GetInputs().find("Size") != program->GetInputs().end()) {
            gl::glVertexAttribPointer(program->GetInputs()["Size"].Location, 2, gl::GL_FLOAT,
                    gl::GL_FALSE, sizeof(TextureBufferData),
                    reinterpret_cast<void *>(offsetof(TextureBufferData, TextureBufferData::Size)));
            gl::glEnableVertexAttribArray(program->GetInputs()["Size"].Location);
            gl::glVertexAttribDivisor(program->GetInputs()["Size"].Location, 1);
        }

        if (program->GetInputs().find("Z") != program->GetInputs().end()) {
            gl::glVertexAttribPointer(program->GetInputs()["Z"].Location, 1, gl::GL_FLOAT,
                    gl::GL_FALSE, sizeof(TextureBufferData),
                    reinterpret_cast<void *>(offsetof(TextureBufferData, TextureBufferData::Z)));
            gl::glEnableVertexAttribArray(program->GetInputs()["Z"].Location);
            gl::glVertexAttribDivisor(program->GetInputs()["Z"].Location, 1);
        }

        gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
        gl::glBindVertexArray(0);
    }
}

void TextureInstanceManager::DrawCall(int count) {
    gl::glMultiDrawArraysIndirect(gl::GL_TRIANGLE_STRIP, 0, count, 0);
}

}  // namespace R3D
