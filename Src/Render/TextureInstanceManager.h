// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_RENDER_TEXTUREINSTANCEMANAGER_H_

#define SRC_RENDER_TEXTUREINSTANCEMANAGER_H_

#include <memory>
#include <utility>
#include <vector>

#include "Render/InstanceManager.h"
#include "Render/Data/Buffer.h"

namespace R3D {

class TextureInstanceManager : public InstanceManager {
 public:
    void Build() override;
    void Purge() override;
    void Update() override;
    void DrawCall(int count) override;

 protected:
    void SetupVertexAttrib(std::pair<Program*, gl::GLuint> pair) override;

    std::unique_ptr<Buffer> data;  ///< Buffer that contains per instance data of all instances
    std::vector<std::unique_ptr<Buffer>> cmdBuffers;  ///< Buffers containing command for indirect rendering, cmdBuffers[i] <==> commands[i], one buffer per technique
    int instanceBuilt;  ///< How many instance there was when we called Build

    /// Represent the data in the data buffer
    struct TextureBufferData {
        glm::vec2 Position;
        glm::vec2 Size;
        glm::vec2 Z;
        gl::GLuint64 Texture;
    };
};

}  // namespace R3D

#endif  // SRC_RENDER_TEXTUREINSTANCEMANAGER_H_
