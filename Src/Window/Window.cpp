// Copyright RariumUnlimited - Licence : MIT
#include "Window/Window.h"

#include <utility>
#include <vector>

#include <Rarium/Tools/Log.h>
#include <Rarium/Tools/Math.h>

#include "App/AsteroidScene.h"
#include "App/GameScene.h"
#include "App/MainMenuScene.h"
#include "Assets/Collection.h"
#include "Assets/FileLoader.h"

namespace R3D {

Window::Window() :
    RR::Window("Rarium3D",
               sf::VideoMode(1280, 720),
               sf::Style::Close,
               sf::ContextSettings(24,
                                   8,
                                   4,
                                   4,
                                   6
#ifdef _DEBUG
                                   , sf::ContextSettings::Attribute::Debug
#endif
                   )) {
    sceneChangeRequested = false;

    if (!renderer.Init(1280, 720)) {
        throw std::string("Renderer Init Error");
    }

    LoadScenes();

    currentSceneDescriptor = scenes[startupSceneName].get();
    currentScene = currentSceneDescriptor->GetScene();
    currentScene->GetUi()->SetUiFunction(this);
    renderer.SetupScene(currentScene.get());
}

Window::~Window() {
    renderer.Unload();
}

void Window::Draw(float delta, sf::RenderTarget& target) {
    if (sceneChangeRequested)
        ChangeScene();

    currentScene->GetUi()->Update(delta,
                                  RR::Window::GetCurrentInput(),
                                  RR::Window::GetLastUpdateInput());
    currentScene->Update(delta,
                         RR::Window::GetCurrentInput(),
                         RR::Window::GetLastUpdateInput());

    renderer.DrawFrame(delta);

    target.draw(*currentScene->GetUi());
}

void Window::ReportError(std::string error) {
    RR::Logger::Log() << "MASSIVE ERROR : " << error << "\n";
    error = true;
}

void Window::ChangeScene() {
    sceneChangeRequested = false;

    Collection::Textures().Purge();
    Collection::Meshes().Purge();
    if (scenes.find(sceneWanted) == scenes.cend()) {
        throw std::string("Trying to request an ui that doesn't exist.");
    } else {
        currentSceneDescriptor = scenes[sceneWanted].get();
        currentScene = currentSceneDescriptor->GetScene();
        currentScene->GetUi()->SetUiFunction(this);
        renderer.SetupScene(currentScene.get());
        RR::Logger::Log() << "Ui Changed : " << sceneWanted << "\n";
    }
}

void Window::RequestSceneChange(std::string scene) {
    if (currentSceneDescriptor->GetName() != scene) {
        sceneChangeRequested = true;
        sceneWanted = scene;
    }
}

void Window::LoadScenes() {
    startupSceneName = "MainMenu";

    scenes.insert(std::make_pair(std::string("MainMenu"),
                                 std::make_unique<MainMenuSceneDescriptor>()));
    scenes.insert(std::make_pair(std::string("Demo2D"),
                                 std::make_unique<GameSceneDescriptor>()));
    scenes.insert(std::make_pair(std::string("Demo3D"),
                                 std::make_unique<AsteroidSceneDescriptor>()));
}

}  // namespace R3D
