// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_WINDOW_WINDOW_H_

#define SRC_WINDOW_WINDOW_H_

#include <map>
#include <memory>
#include <string>

#include <glm/glm.hpp>
#include <Rarium/Window/Window.h>
#include <SFML/Graphics.hpp>

#include "Assets/Scene/Scene.h"
#include "Render/Renderer.h"
#include "Window/WindowFunction.h"

namespace R3D {

class Window : public RR::Window, public R3D::WindowFunction {
 public:
    /**
     *    Build the engine
     *    @param appLogic Update logic for the user app
     */
    Window();
    /**
     *    Destroy our engine
     */
    ~Window();

    /**
     *    Draw what needs to be drawn
     */
    void Draw(float delta, sf::RenderTarget& target) override;

    /**
     *    Report an error to the engine
     *    @param error The message of the error
     */
    static void ReportError(std::string error);

    // ===============<UiFunction>====================
    void RequestSceneChange(std::string scene) override;
    const RR::InputState& GetInput() override { return GetCurrentInput(); }
    const RR::InputState& GetLastInput() override { return GetLastUpdateInput(); }
    // ===============<UiFunction>====================

 protected:
    Renderer renderer;  ///< OpenGL renderer used to draw uis

    sf::RenderWindow mainwindow;  ///< Our sfml window
    sf::ContextSettings maincontext;  ///< Our opengl context

    // ===============<Ui Management>====================

    /**
     *    Load all the scene descriptors in memory
     */
    void LoadScenes();

    std::string startupSceneName;  ///< Name of the scene to display at the application startup


    std::map<std::string, std::unique_ptr<SceneDescriptor>> scenes;  ///< List of all scenes, Name -> Descriptor

    std::unique_ptr<Scene> currentScene;  ///< Pointer to the currently displayed scene
    SceneDescriptor* currentSceneDescriptor;  ///< Pointer to the current scene descriptor


    /**
     *    Change the current scene displayed
     */
    void ChangeScene();
    bool sceneChangeRequested;  ///< If a scene change has been requested
    std::string sceneWanted;  ///< Name of the scene to change to
    // ===============<Ui Management>====================
};

}  // namespace R3D

#endif  // SRC_WINDOW_WINDOW_H_
