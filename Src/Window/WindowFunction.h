// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_WINDOW_WINDOWFUNCTION_H_

#define SRC_WINDOW_WINDOWFUNCTION_H_

#include <string>

#include <Rarium/Window/InputState.h>

namespace R3D {

/**
 *	Object that contains function related to ui manipulation
 */
class WindowFunction {
 public:
    /**
     *	Request to change the currently displayed and active scene
     *	@param pScene Name of the new scene to display
     */
    virtual void RequestSceneChange(std::string pScene) = 0;
    /// Get the state of user input in the current frame
    virtual const RR::InputState& GetInput() = 0;
    /// Get the state of user input in the last frame
    virtual const RR::InputState& GetLastInput() = 0;
};

}  // namespace R3D

#endif  // SRC_WINDOW_WINDOWFUNCTION_H_
