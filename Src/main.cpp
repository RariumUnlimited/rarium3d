// Copyright RariumUnlimited - Licence : MIT
#include <iostream>

#include "Window/Window.h"

int main(int argc, char* args[]) {
    try {
        R3D::Window window;

        window.Run();
    } catch (std::string s) {
        std::cout << s << std::endl;
    }

    return 0;
}
